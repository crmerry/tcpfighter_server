#pragma once

class RingBuffer;

/* ***************************************
구조체
**************************************** */
struct Session
{
	SOCKET			_socket;
	SOCKADDR_IN		_address;

	RingBuffer*		_recv_Q;	
	RingBuffer*		_send_Q;
	unsigned int	_last_packet_time;
	unsigned int	_current_packet_time;
	unsigned int	_last_attack_time;
	int				_ID;

	bool			_b_to_be_disconnect; //센드 큐에 있는 것을 보내고 난 뒤에 끊기 위해, 한 번 늦게 끊음.
	bool			_b_right_disconnect; //센드 큐 상관 없이 바로 끊어버림.
};

/* ***************************************
세션 관리 함수
**************************************** */
void InitializeSession(Session* const session);

bool CreateSession(Session** out_sessionsession);
void DeleteSession(Session* const session);

bool AddSession(Session* const session);
bool RemoveSession(Session* const session);

bool FindSession(Session** session, SOCKET socket);

void MakeSessionDisconnectThisTime(Session* const session);
void MakeSessionDisconnectNextTime(Session* const session);

void DisconnectSession(Session* const session);
void DisconnectAllSession();

void UpdateRecvPacketTime(Session* const session);

//unsigned char	_direction;
//unsigned short	_x;
//unsigned short	_y;
//unsigned char	_HP;
//unsigned char	_action;


//unsigned int	_prev_attack_time = 0;

//int	_sector_col;
//int	_sector_row;