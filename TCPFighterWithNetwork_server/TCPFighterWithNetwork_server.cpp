/* ***************************************
NOTE: 정리
1. 
프레임이 너무 밀려서 서버에서 점프를 할 때, 섹터 2개 이상을 넘어가면(섹터 1개까진 괜찮...) 문제가 생기게됨. 
섹터 2개를 넘어가면 leaveleft라도 왼쪽 뿐만 아니라, 위 아래에도 뿌려줘야 함.

2. 
Sync 패킷은 주로, rtt가 높아지면서 frame이 떨어지면 발생.

3.
브로드캐스트는 발생할 때마다, 모든 세션의 센드 버퍼에 넣는 것이 아니고,
전역 브로드캐스트용 센드 버퍼를 만들고, 이 버퍼에 삽입한 후, 네트워크 프로시져에서 일괄적으로 전송.
단, 한 번에 모든 세션에 전송하는 것이 아니고, select를 나누어서 하듯이, 브로드캐스트도 나눔(why? 총 부하는 같지만, 부하를 나누어서 받는 것)
**************************************** */

#include "stdafx.h"
#include "Contents.h"
#include "Protocol.h"
#include "Log.h"
#include "RingBuffer.h"
#include "Session.h"
#include "Character.h"
#include "Sector.h"
#include "NetworkProcedure.h"
#include "Profiler.h"

/* ***************************************
라이브러리
**************************************** */
#pragma comment(lib, "Winmm.lib")
#pragma comment(lib, "Ws2_32.lib")

/* ***************************************
매크로
**************************************** */

/* ***************************************
전역변수
**************************************** */
bool	g_b_server_run;
SOCKET	g_listen_socket;
int		g_ID = 1;

std::map <SOCKET, Session*> g_session_map;

std::map <int, Character*> g_character_map;

std::list<int> g_sector_arr[SECTOR_MAX_Y + 1][SECTOR_MAX_X + 1];

void(*CalculateLeavSector[9])(TargetSector*, short, short);
void(*CalculateEnterSector[9])(TargetSector*, short, short);

SetOfTargetSector g_sector_info[SECTOR_MAX_Y + 1][SECTOR_MAX_X + 1];

int g_sync_call;
/* ***************************************
브로드캐스트용 전역변수.
except_list에는 각 패킷의 사이즈와 제외될 ID가 들어가 있음.
**************************************** */
std::list<BroadCastUnit> g_broadcast_except_list;
RingBuffer g_broadcast_Q;

/* ***************************************
전역함수
**************************************** */

/* ***************************************
초기화 & 정리 함수.
**************************************** */
bool InitializeGameServer();
void ReleaseGameServer();

bool InitializeEnvrionment();
bool InitializeNetwork();
void InitializeGameData();

void ReleaseEnvrionment();
void ReleaseNetwork();

void TerminateServer();

/* ***************************************
유틸 함수
**************************************** */
void KeyControl();
bool CompareServerAndClientPosition(unsigned short client_x, unsigned short client_y, Character* character);
bool ComparePosition(short pos_x1, short pos_y1, short pos_x2, short pos_y2);
bool CheckAttack(int* ID, Character* character, TargetSector* search_sector);
bool DeadReckoning(int* out_estimate_x, int* out_estimate_y, Character* const character);
void SortSectorInfo(TargetSector* sort_arr);

/* ***************************************
네트워크
**************************************** */
void NetworkMainProc();
void DistributeSessionsToSelect();
void CallSelect(Session** sessions, FD_SET* r_set, FD_SET* w_set, size_t count);

/* ***************************************
게임 루프
**************************************** */
void UpdateGame();
void Action();
bool FrameSkip();
void MoveCharacter(Character* const character, Session* const session, int dif_time);
void ChangeSector(Character* const character);

int main()
{
	g_b_server_run = InitializeGameServer();
	
	while (g_b_server_run)
	{
		KeyControl();

		NetworkMainProc();

		UpdateGame();
	}

	ReleaseGameServer();

	return 0;
}

/* ***************************************
초기화 & 정리 함수.
**************************************** */
bool InitializeGameServer()
{
	PROFILE_INITIALIZE;

	if (!InitializeLog())
	{
		LOG(LOG_LEVEL_ERROR, L"Can not initialize log");

		return false;
	}

	if (!InitializeEnvrionment())
	{
		LOG(LOG_LEVEL_ERROR, L"Can not initialize envrionment");

		return false;
	}

	if (!InitializeNetwork())
	{
		LOG(LOG_LEVEL_ERROR, L"Can not initialize network");

		return false;
	}

	InitializeGameData();

	/* ***************************************
	시스템 데이터 로그
	**************************************** */
	LOG(LOG_LEVEL_SYSTEM, L"InitializeGameServer--------------------------------");
	LOG(LOG_LEVEL_SYSTEM, L"IP : %s, port : %d ", SERVER_IP_ANY, SERVER_PORT);
	LOG(LOG_LEVEL_SYSTEM, L"limit user count : %d", SESSION_COUNT_MAX);
	LOG(LOG_LEVEL_SYSTEM, L"LOG_LEVEL : %d", g_log_level);
	LOG(LOG_LEVEL_SYSTEM, L"FPS : %d", FRAME_PER_SECOND);
	LOG(LOG_LEVEL_SYSTEM, L"Recv Queue size : %d", RECV_QUEUE_SIZE);
	LOG(LOG_LEVEL_SYSTEM, L"Send Queue size : %d", SEND_QUEUE_SIZE);
	LOG(LOG_LEVEL_SYSTEM, L"Sector size : %d", SECTOR_SIZE);
	LOG(LOG_LEVEL_SYSTEM, L"Sector x(max) : %d", SECTOR_MAX_X);
	LOG(LOG_LEVEL_SYSTEM, L"Sector y(max) : %d", SECTOR_MAX_Y);
	LOG(LOG_LEVEL_SYSTEM, L"----------------------------------------------------");
	return true;
}

void ReleaseGameServer()
{
	/* ***************************************
	시스템 데이터 로그
	**************************************** */
	int user_count_in_sector = 0;
	size_t session_count = g_session_map.size();
	size_t character_count = g_character_map.size();

	for (int row = 0; row < SECTOR_MAX_Y + 1; row++)
	{
		for (int col = 0; col < SECTOR_MAX_X + 1; col++)
		{
			user_count_in_sector += (int)g_sector_arr[row][col].size();
		}
	}

	LOG(LOG_LEVEL_SYSTEM, L"ReleaseGameServer-----------------------------------");
	LOG(LOG_LEVEL_SYSTEM, L"session count : %I64u", session_count);
	LOG(LOG_LEVEL_SYSTEM, L"user count : %I64u", character_count);
	LOG(LOG_LEVEL_SYSTEM, L"max user count : %d", g_ID - 1);
	LOG(LOG_LEVEL_SYSTEM, L"sync_packet_count : %d", g_sync_call);
	LOG(LOG_LEVEL_SYSTEM, L"user count in sector : %d", user_count_in_sector);
	LOG(LOG_LEVEL_SYSTEM, L"----------------------------------------------------");

	for (int row = 0; row < SECTOR_MAX_Y + 1; row++)
	{
		for (int col = 0; col < SECTOR_MAX_X + 1; col++)
		{
			auto iter = g_sector_arr[row][col].begin();
			auto end = g_sector_arr[row][col].end();

			if (iter != end)
			{
				LOG(LOG_LEVEL_SYSTEM, L"Sector [%d][%d]", row, col);
			}
			
			while (iter != end)
			{
				LOG(LOG_LEVEL_SYSTEM, L"(%d) ", *iter);

				++iter;
			}
		}
	}

	

	DisconnectAllSession();
	ReleaseEnvrionment();
	ReleaseNetwork();
	ReleaseLog();

	PROFILE_OUT;
}

bool InitializeEnvrionment()
{
	_wsetlocale(LC_ALL, L"korean");

	return (timeBeginPeriod(1) == TIMERR_NOERROR);
}

void ReleaseEnvrionment()
{
	timeEndPeriod(1);
}

bool InitializeNetwork()
{
	do
	{
		WSADATA wsa;
		int error;

		if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
		{
			LOG(LOG_LEVEL_ERROR, L"WSAStartup error");

			break;
		}

		g_listen_socket = socket(AF_INET, SOCK_STREAM, 0);

		if (g_listen_socket == INVALID_SOCKET)
		{
			LOG(LOG_LEVEL_ERROR, L"Listen socket can not be created");

			break;
		}

		/* ***************************************
		소켓 옵션 변경
		**************************************** */
		u_long nonblocking_on = 1;
		error = ioctlsocket(g_listen_socket, FIONBIO, &nonblocking_on);

		if (error == SOCKET_ERROR)
		{
			LOG(LOG_LEVEL_ERROR, L"Can not change block socket to non block socket");

			break;
		}

		/* ***************************************
		윈도우 소켓 전용 TCP_KEEPALIVE 옵션 주기.
		기존의 TCP_KEEPALIVE는 주기가 너무 길다.
		그러므로 아래의 함수를 사용.
		NOTE: 테스트할 동안은 off..
		**************************************** */
		/*DWORD keep_alive_return;
		tcp_keepalive keep_alive_opt{1, TCP_KEEP_ALIVE_TIME, TCP_KEEP_ALIVE_INTERVAL};

		error = WSAIoctl(g_listen_socket, SIO_KEEPALIVE_VALS, &keep_alive_opt, sizeof keep_alive_opt, NULL, 0, &keep_alive_return, NULL, NULL);

		if (error == SOCKET_ERROR)
		{
			LOG(LOG_LEVEL_ERROR, L"Can not change SIO_KEEPALIVE_VALS option");

			break;
		}*/

		SOCKADDR_IN server_addr;
		ZeroMemory(&server_addr, sizeof server_addr);

		server_addr.sin_port = htons(SERVER_PORT);
		server_addr.sin_family = AF_INET;
		error = InetPton(AF_INET, SERVER_IP_ANY, &server_addr.sin_addr);

		/* ***************************************
		error 값이 1이면 정상, 0 이면 잘못된 IP 주소, -1인 경우 에러 코드 확인.
		**************************************** */
		if (error == 0)
		{
			LOG(LOG_LEVEL_ERROR, L"IP is wrong");

			break;
		}
		else if (error == -1)
		{
			error = WSAGetLastError();
			LOG(LOG_LEVEL_ERROR, L"InetPton error. code : %d ", error);

			break;
		}

		error = bind(g_listen_socket, (SOCKADDR*)&server_addr, sizeof server_addr);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();
			LOG(LOG_LEVEL_ERROR, L"Listen socket bind error. code : %d ", error);

			break;
		}

		error = listen(g_listen_socket, SOMAXCONN);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();
			LOG(LOG_LEVEL_ERROR, L"Listen socket listen error. code : %d ", error);

			break;
		}

		return true;

	} while (0);

	return false;
}

void InitializeGameData()
{
	CalculateLeavSector[0] = CalculateLeaveRightDownSector;
	CalculateLeavSector[1] = CalculateLeaveDownSector;
	CalculateLeavSector[2] = CalculateLeaveLeftDownSector;
	CalculateLeavSector[3] = CalculateLeaveRightSector;
	CalculateLeavSector[4] = CalculateLeaveCurrentSector;
	CalculateLeavSector[5] = CalculateLeaveLeftSector;
	CalculateLeavSector[6] = CalculateLeaveRightUpSector;
	CalculateLeavSector[7] = CalculateLeaveUpSector;
	CalculateLeavSector[8] = CalculateLeaveLeftUpSector;

	CalculateEnterSector[0] = CalculateEnterLeftUpSector;
	CalculateEnterSector[1] = CalculateEnterUpSector;
	CalculateEnterSector[2] = CalculateEnterRightUpSector;
	CalculateEnterSector[3] = CalculateEnterLeftSector;
	CalculateEnterSector[4] = CalculateEnterCurrentSector;
	CalculateEnterSector[5] = CalculateEnterRightSector;
	CalculateEnterSector[6] = CalculateEnterLeftDownSector;
	CalculateEnterSector[7] = CalculateEnterDownSector;
	CalculateEnterSector[8] = CalculateEnterRightDownSector;

	/* ***************************************
	섹터맵인포
	**************************************** */
	memset(g_sector_info, -2, sizeof g_sector_info);
	int count = 0;

	for (short y = 0; y < SECTOR_MAX_Y + 1; y++)
	{
		for (short x = 0; x < SECTOR_MAX_X + 1; x++)
		{
			/* ***************************************
			
			**************************************** */
			g_sector_info[y][x]._set_of_sectors[0]._size = 5;
			g_sector_info[y][x]._set_of_sectors[0]._sector[0] = { x - 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[0]._sector[1] = { x - 1, y };
			g_sector_info[y][x]._set_of_sectors[0]._sector[2] = { x - 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[0]._sector[3] = { x , y - 1 };
			g_sector_info[y][x]._set_of_sectors[0]._sector[4] = { x + 1 , y - 1 };

			g_sector_info[y][x]._set_of_sectors[1]._size = 3;
			g_sector_info[y][x]._set_of_sectors[1]._sector[0] = { x - 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[1]._sector[1] = { x , y - 1 };
			g_sector_info[y][x]._set_of_sectors[1]._sector[2] = { x + 1, y - 1 };

			g_sector_info[y][x]._set_of_sectors[2]._size = 5;
			g_sector_info[y][x]._set_of_sectors[2]._sector[0] = { x - 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[2]._sector[1] = { x , y - 1 };
			g_sector_info[y][x]._set_of_sectors[2]._sector[2] = { x + 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[2]._sector[3] = { x + 1, y };
			g_sector_info[y][x]._set_of_sectors[2]._sector[4] = { x + 1, y + 1 };

			g_sector_info[y][x]._set_of_sectors[3]._size = 3;
			g_sector_info[y][x]._set_of_sectors[3]._sector[0] = { x - 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[3]._sector[1] = { x - 1, y };
			g_sector_info[y][x]._set_of_sectors[3]._sector[2] = { x - 1, y + 1 };

			g_sector_info[y][x]._set_of_sectors[4]._size = 9;
			g_sector_info[y][x]._set_of_sectors[4]._sector[0] = { x - 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[4]._sector[1] = { x, y - 1 };
			g_sector_info[y][x]._set_of_sectors[4]._sector[2] = { x + 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[4]._sector[3] = { x + 1, y };
			g_sector_info[y][x]._set_of_sectors[4]._sector[4] = { x + 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[4]._sector[5] = { x , y + 1 };
			g_sector_info[y][x]._set_of_sectors[4]._sector[6] = { x - 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[4]._sector[7] = { x - 1, y };
			g_sector_info[y][x]._set_of_sectors[4]._sector[8] = { x , y };

			g_sector_info[y][x]._set_of_sectors[5]._size = 3;
			g_sector_info[y][x]._set_of_sectors[5]._sector[0] = { x + 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[5]._sector[1] = { x + 1, y };
			g_sector_info[y][x]._set_of_sectors[5]._sector[2] = { x + 1, y + 1 };

			g_sector_info[y][x]._set_of_sectors[6]._size = 5;
			g_sector_info[y][x]._set_of_sectors[6]._sector[0] = { x - 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[6]._sector[1] = { x - 1, y };
			g_sector_info[y][x]._set_of_sectors[6]._sector[2] = { x - 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[6]._sector[3] = { x , y + 1 };
			g_sector_info[y][x]._set_of_sectors[6]._sector[4] = { x + 1, y + 1 };

			g_sector_info[y][x]._set_of_sectors[7]._size = 3;
			g_sector_info[y][x]._set_of_sectors[7]._sector[0] = { x - 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[7]._sector[1] = { x , y + 1 };
			g_sector_info[y][x]._set_of_sectors[7]._sector[2] = { x + 1, y + 1 };

			g_sector_info[y][x]._set_of_sectors[8]._size = 5;
			g_sector_info[y][x]._set_of_sectors[8]._sector[0] = { x + 1, y - 1 };
			g_sector_info[y][x]._set_of_sectors[8]._sector[1] = { x + 1, y };
			g_sector_info[y][x]._set_of_sectors[8]._sector[2] = { x + 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[8]._sector[3] = { x - 1, y + 1 };
			g_sector_info[y][x]._set_of_sectors[8]._sector[4] = { x , y + 1 };
		}
	}

	for (short y = 0; y < SECTOR_MAX_Y + 1; y++)
	{
		for (short x = 0; x < SECTOR_MAX_X + 1; x++)
		{
			for (short count = 0; count < 9; count++)
			{
				SortSectorInfo(&g_sector_info[y][x]._set_of_sectors[count]);
			}
		}
	}
}

void ReleaseNetwork()
{
	closesocket(g_listen_socket);
	WSACleanup();
}

void TerminateServer()
{
	g_b_server_run = false;
}

/* ***************************************
유틸 함수
**************************************** */
void KeyControl()
{
	static bool b_control_onoff = false;
	static unsigned int last_call = GetTickCount();
	unsigned int current_call = GetTickCount();

	do
	{
		if (current_call - last_call < 1000)
		{
			break;
		}

		if (GetAsyncKeyState(VK_F1) & 0x8001 && GetAsyncKeyState(0x55) & 0x8001) // 0x55 : u
		{
			b_control_onoff = true;
		}

		if (GetAsyncKeyState(VK_F2) & 0x8001 && GetAsyncKeyState(0x4c) & 0x8001) // 0x4c :L
		{
			b_control_onoff = false;
		}


		if (GetAsyncKeyState(0x50) & 0x8001 && b_control_onoff) // 0x50 : p
		{
			#ifdef _DEBUG
			wchar_t debug_string[100];
			debug_string[99] = L'\0';

			for (int y = 0; y <= SECTOR_MAX_Y; y++)
			{
				for (int x = 0; x <= SECTOR_MAX_X; x++)
				{
					auto iter = g_sector_arr[y][x].begin();
					auto end = g_sector_arr[y][x].end();

					if (iter != end)
					{
						swprintf_s(debug_string, 100, L"\n# Sector (x : %d, y : %d) \n", x, y);
						OutputDebugString(debug_string);
					}

					while (iter != end)
					{
						swprintf_s(debug_string, 100, L"[%d] ", *iter);
						OutputDebugString(debug_string);

						++iter;
					}
				}
			}
			#endif // _DEBUG
		}

		if (GetAsyncKeyState(0x51) & 0x8001 && b_control_onoff) // 0x51 : q
		{
			TerminateServer();
		}

		last_call = current_call;
		
	} while (0);
}

bool CompareServerAndClientPosition(unsigned short client_x, unsigned short client_y, Character* character)
{
	int dif_x = abs((short)character->_pos_x - (short)client_x);
	int dif_y = abs((short)character->_pos_y - (short)client_y);

	return (dif_x < MOVE_LIMIT_DISTANCE && dif_y < MOVE_LIMIT_DISTANCE);
}

bool ComparePosition(short pos_x1, short pos_y1, short pos_x2, short pos_y2)
{
	int dif_x = abs(pos_x1 - pos_x2);
	int dif_y = abs(pos_y1 - pos_y2);

	return (dif_x < MOVE_LIMIT_DISTANCE && dif_y < MOVE_LIMIT_DISTANCE);
}

bool CheckAttack(int* ID, Character* character, TargetSector* search_sector)
{
	int x_min = 0;
	int x_max = 0;
	int y_min = 0;
	int y_max = 0;

	/* ***************************************
	캐릭터 위치의 위~아래 50만큼을 범위로 잡음.
	**************************************** */
	y_min = character->_pos_y - 25;
	y_max = character->_pos_y + 25;

	/* ***************************************
	방향에 따라 좌우 범위 설정.
	**************************************** */
	if (character->_direction == ACTION_MOVE_RR)
	{
		x_min = character->_pos_x + 10;
		x_max = character->_pos_x + 70;
	}
	else if (character->_direction == ACTION_MOVE_LL)
	{
		x_min = character->_pos_x - 70;
		x_max = character->_pos_x - 10;
	}

	/* ***************************************
	주변 섹터에 해당하는 유저가 있는지 검색.
	현재, 한 명의 유저에게만 공격 가능.
	**************************************** */
	for (int count = 0; count < search_sector->_size; count++)
	{
		auto iter = g_sector_arr[search_sector->_sector[count]._y][search_sector->_sector[count]._x].begin();
		auto end = g_sector_arr[search_sector->_sector[count]._y][search_sector->_sector[count]._x].end();

		while (iter != end)
		{
			Character* target_character = nullptr;

			FindCharacter(&target_character, *iter);

			if (!target_character)
			{
				LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d", WFILE, __LINE__);

				TerminateServer();

				return false;
			}

			if (target_character->_pos_x > x_min && target_character->_pos_x < x_max && target_character->_pos_y > y_min && target_character->_pos_y < y_max)
			{
				/* ***************************************
				찾았으면 그대로 리턴.
				**************************************** */
				*ID = target_character->_ID;

				return true;
			}
			else
			{
				++iter;
			}
		}
	}

	return false;
}

bool DeadReckoning(int* out_estimate_x, int* out_estimate_y, Character* const character)
{
	/* ***************************************
	액션을 시작했을 때의 위치 + 움직임 액션 + 방향 + 지난 시간을 통하여
	캐릭터의 위치를 추측.
	**************************************** */
	unsigned char action = character->_action;
	
	short last_action_x = character->_action_x;
	short last_action_y = character->_action_y;

	short move_distance_x = MOVE_DISTANCE_X;
	short move_distance_y = MOVE_DISTANCE_Y;

	short estimate_x = 0;
	short estimate_y = 0;

	/* ***************************************
	시간 간격을 통하여 몇 프레임인지 계산.
	**************************************** */
	int dif_time = character->_session->_current_packet_time - character->_session->_last_packet_time;
	
	if (dif_time < SECOND_PER_FRAME)
	{
		*out_estimate_x = last_action_x;
		*out_estimate_y = last_action_y;

		return false;
	}

	int multiply = (dif_time / SECOND_PER_FRAME);

	/* ***************************************
	이동 거리는 프레임 * 프레임당 이동 거리.
	**************************************** */
	move_distance_x *= multiply;
	move_distance_y *= multiply;

	/* ***************************************
	액션에 따라, 추정된 위치를 구해준다.
	**************************************** */
	switch (action)
	{
	case ACTION_MOVE_LL:
		estimate_x = last_action_x - move_distance_x;
		estimate_y = last_action_y;

		if (estimate_x <= MAP_MOVE_LEFT)
		{
			estimate_x = MAP_MOVE_LEFT + 1;
		}
		
		break;

	case ACTION_MOVE_LU:
		estimate_x = last_action_x - move_distance_x;
		estimate_y = last_action_y - move_distance_y;

		if (estimate_x <= MAP_MOVE_LEFT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = last_action_x;		
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_LEFT +1;
			estimate_y = max(last_action_y - dif_y2, MAP_MOVE_TOP + 1);
		}
		else if (estimate_y <= MAP_MOVE_TOP)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = max(MAP_MOVE_LEFT + 1, last_action_x - dif_x2);
			estimate_y = MAP_MOVE_TOP + 1;
		}
		
		break;

	case ACTION_MOVE_LD:
		estimate_x = last_action_x - move_distance_x;
		estimate_y = last_action_y + move_distance_y;

		if (estimate_x <= MAP_MOVE_LEFT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_LEFT + 1;
			estimate_y = min(last_action_y + dif_y2, MAP_MOVE_BOTTOM - 1);
		}
		else if (estimate_y >= MAP_MOVE_BOTTOM)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = MAP_MOVE_BOTTOM - last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = max(MAP_MOVE_LEFT + 1, last_action_x - dif_x2);
			estimate_y = MAP_MOVE_BOTTOM - 1;
		}

		break;

	case ACTION_MOVE_RR:
		estimate_x = last_action_x + move_distance_x;
		estimate_y = last_action_y;

		if (estimate_x >= MAP_MOVE_RIGHT)
		{
			estimate_x = MAP_MOVE_RIGHT - 1;
		}

		break;

	case ACTION_MOVE_RU:
		estimate_x = last_action_x + move_distance_x;
		estimate_y = last_action_y - move_distance_y;

		if (estimate_x >= MAP_MOVE_RIGHT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = MAP_MOVE_RIGHT - last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_RIGHT - 1;
			estimate_y = max(last_action_y - dif_y2, MAP_MOVE_TOP + 1);
		}
		else if (estimate_y <= MAP_MOVE_TOP)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = min(MAP_MOVE_RIGHT - 1, last_action_x + dif_x2);
			estimate_y = MAP_MOVE_TOP + 1;
		}

		break;

	case ACTION_MOVE_RD:
		estimate_x = last_action_x + move_distance_x;
		estimate_y = last_action_y + move_distance_y;

		if (estimate_x >= MAP_MOVE_RIGHT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = MAP_MOVE_RIGHT - last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_RIGHT - 1;
			estimate_y = min(last_action_y + dif_y2, MAP_MOVE_BOTTOM - 1);
		}
		else if (estimate_y >= MAP_MOVE_BOTTOM)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = MAP_MOVE_BOTTOM - last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = min(MAP_MOVE_RIGHT - 1, last_action_x + dif_x2);
			estimate_y = MAP_MOVE_BOTTOM - 1;
		}

		break;

	case ACTION_MOVE_UU:
		estimate_x = last_action_x;
		estimate_y = last_action_y - move_distance_y;
		
		if (estimate_y <= MAP_MOVE_TOP)
		{
			estimate_y = MAP_MOVE_TOP + 1;
		}

		break;

	case ACTION_MOVE_DD:
		estimate_x = last_action_x;
		estimate_y = last_action_y + move_distance_y;

		if (estimate_y >= MAP_MOVE_BOTTOM)
		{
			estimate_y = MAP_MOVE_BOTTOM - 1;
		}
		break;

	case ACTION_STAND:
		estimate_x = last_action_x;
		estimate_y = last_action_y;
		break;

	default:
		LOG(LOG_LEVEL_ERROR, L"Wrong Action type. type : %d, socket : %I64u, ID : %d", action, character->_session->_socket, character->_ID);
		MakeSessionDisconnectThisTime(character->_session);
		break;
	}

	/* ***************************************
	out of range 체크
	**************************************** */
	estimate_x = max(MAP_MOVE_LEFT + 1, estimate_x);
	estimate_x = min(MAP_MOVE_RIGHT - 1, estimate_x);
	estimate_y = max(MAP_MOVE_TOP + 1, estimate_y);
	estimate_y = min(MAP_MOVE_BOTTOM - 1, estimate_y);

	*out_estimate_x = estimate_x;
	*out_estimate_y = estimate_y;

	/* ***************************************
	추정치와 실제 위치를 비교한 값을 리턴.
	**************************************** */
	return ComparePosition(character->_pos_x, character->_pos_y, estimate_x, estimate_y);
}

void SortSectorInfo(TargetSector* sort_arr)
{
	int size = sort_arr->_size;

	for (int index = 0; index < size; index++)
	{
		if (sort_arr->_sector[index]._x > SECTOR_MAX_X || sort_arr->_sector[index]._y > SECTOR_MAX_Y)
		{
			sort_arr->_sector[index]._x = -2;
			sort_arr->_sector[index]._y = -2;
		}
	}

	for (int i = size - 1; i >= 0; i--)
	{
		for (int j = i; j >= 0; j--)
		{
			if (sort_arr->_sector[j]._x < 0 || sort_arr->_sector[j]._y < 0)
			{
				Sector temp = sort_arr->_sector[i];
				sort_arr->_sector[i] = sort_arr->_sector[j];
				sort_arr->_sector[j] = temp;
			}
		}
	}
	int count = 0;

	for (int index = 0; index < size; index++)
	{
		if (sort_arr->_sector[index]._x >= 0 && sort_arr->_sector[index]._y >= 0)
		{
			count++;
		}
		else
		{
			break;
		}
	}

	sort_arr->_size = count;
}

/* ***************************************
네트워크 메인
**************************************** */
void NetworkMainProc()
{
	DistributeSessionsToSelect();
}

/* ***************************************
select는 FD_SETSIZE에 의하여 64개의 소켓만
조사가 가능하다. FD_SETSIZE를 늘려주면
한 번에 select할 수 있는 양을 늘려줄 수 있지만,
검색 자체에 너무 많은 시간을 투자하게 되어
반응성이 느려질 수 있다.
따라서 select을 나누어서 해줌.
**************************************** */
void DistributeSessionsToSelect()
{
	/* ***************************************
	세션 최대 숫자 만큼을 배열로 설정.
	select을 세션을 FD_SETSIZE(64)만큼 나누어서 진행.
	**************************************** */
	Session* sessions[SESSION_COUNT_MAX + 1];

	/* ***************************************
	brodcast_packet_size는 브로드 캐스트 버퍼에
	있는 패킷 하나의 사이즈. 네트쿼으 프로시저 진행중에
	브로드 캐스트 버퍼에 패킷이 추가되더라도 변하지 않도록, 
	현재 단계에서 설정. 
	**************************************** */
	int broadcast_packet_size = 0;

	if (g_broadcast_except_list.size() > 0)
	{
		broadcast_packet_size = g_broadcast_except_list.front()._packet_size;
	}

	auto session_iter = g_session_map.begin();
	auto session_end = g_session_map.end();

	size_t session_size = g_session_map.size();
	size_t index = 0;

	/* ***************************************
	세션 리스트에서 세션들을 배열에 대입.
	**************************************** */
	while (session_iter != session_end)
	{
		sessions[index] = session_iter->second;

		++session_iter;
		++index;
	}

	/* ***************************************
	fd_set_size 만큼 나누어서 select를 호출.
	**************************************** */
	const size_t fd_set_size = SELECT_SIZE;
	size_t loop_max = session_size / fd_set_size;
	size_t loop_left = session_size % fd_set_size;

	FD_SET r_set;
	FD_SET w_set;

	size_t sessions_index = 0;

	for (unsigned int index = 0; index < loop_max; index++)
	{
		sessions_index = index*fd_set_size;

		if (broadcast_packet_size)
		{
			SendProcBroadcastPacketByDistribute(&sessions[sessions_index], fd_set_size, broadcast_packet_size, false);
		}

		FD_ZERO(&r_set);
		FD_ZERO(&w_set);

		for (unsigned int count = 0; count < fd_set_size; count++)
		{
			if (sessions[count + sessions_index])
			{
				FD_SET(sessions[count + sessions_index]->_socket, &r_set);

				if (sessions[count + sessions_index]->_send_Q->GetUsedSize())
				{
					FD_SET(sessions[count + sessions_index]->_socket, &w_set);
				}
			}
		}

		CallSelect(&sessions[sessions_index], &r_set, &w_set, fd_set_size);
	}

	/* ***************************************
	나머지 호출
	**************************************** */
	FD_ZERO(&r_set);
	FD_ZERO(&w_set);

	/* ***************************************
	리슨 소켓 설정
	**************************************** */
	FD_SET(g_listen_socket, &r_set);

	sessions_index = loop_max*fd_set_size;

	if (broadcast_packet_size)
	{
		/* ***************************************
		마지막 브로드 캐스트 이므로, 패킷을 보내준 후
		마지막 인자를 true로 하여, 보낸 만큼 땡겨주도록함.
		**************************************** */
		SendProcBroadcastPacketByDistribute(&sessions[sessions_index], loop_left, broadcast_packet_size, true);
	}

	for (unsigned int index = 0; index < loop_left; index++)
	{
		if (sessions[index + sessions_index])
		{
			FD_SET(sessions[index + sessions_index]->_socket, &r_set);

			if (sessions[index + sessions_index]->_send_Q->GetUsedSize())
			{
				FD_SET(sessions[index + sessions_index]->_socket, &w_set);
			}
		}
	}

	/* ***************************************
	일반 적으로, loop_left가 0인 경우(즉, r_set과 w_set에 들어간 소켓이 없는 경우)에 CallSelect를 호출하면, selec함수가 에러를 리턴한다.
	때문에, loop_left가 0인 경우를 걸러줘야 한다. 그러나, 위에서 listen 소켓을 매번 넣으므로, r_set에 listen socket이 계속 들어가게 되어,
	문제 없이 진행 가능.
	**************************************** */
	CallSelect(&sessions[sessions_index], &r_set, &w_set, loop_left);
}

void CallSelect(Session** sessions, FD_SET* r_set, FD_SET* w_set, size_t count)
{
	timeval time{ 0,0 };

	int ret_val = select(0, r_set, w_set, NULL, &time);

	if (ret_val > 0)
	{
		if (FD_ISSET(g_listen_socket, r_set))
		{
			Session* session = nullptr;
			Character* character = nullptr;

			if (CreateSession(&session) && CreateCharacter(&character, session))
			{
				NetCreateMyCharacterSC(session);
				NetInformSessionOfNeighbors(session);
				NetInformNeighborsOfSession(session);
				SetSector(character);
			}
		}

		for (unsigned int index = 0; index < count; index++)
		{
			if (FD_ISSET(sessions[index]->_socket, r_set))
			{
				RecvProc(sessions[index]);
			}

			if (FD_ISSET(sessions[index]->_socket, w_set))
			{
				SendProc(sessions[index]);

				/* ***************************************
				보내고 끊기를 위한 설정.
				**************************************** */
				if (sessions[index]->_b_to_be_disconnect)
				{
					DisconnectSession(sessions[index]);

					continue;
				}
			}

			/* ***************************************
			바로 끊어 준다.
			**************************************** */
			if (sessions[index]->_b_right_disconnect)
			{
				DisconnectSession(sessions[index]);
			}
		}
	}
	else if (ret_val == SOCKET_ERROR)
	{
		int error = WSAGetLastError();
		
		LOG(LOG_LEVEL_ERROR, L"select error. error code : %d, select_size : %I64u ", error, count);

		TerminateServer();
	}
}

/* ***************************************
게임 루프
**************************************** */
void UpdateGame()
{
	if (FrameSkip())
	{
		return;
	}

	Action();
}

void Action()
{
	static unsigned int prev = timeGetTime();
	unsigned int cur = timeGetTime();
	unsigned int dif_time = cur - prev;
	prev = cur;

	auto iter = g_session_map.begin();
	auto end = g_session_map.end();

	while (iter != end)
	{
		Session* session = iter->second;
		Character* character = nullptr;

		FindCharacter(&character, session->_ID);

		if (!character)
		{
			LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

			++iter;
			DisconnectSession(session);

			continue;
		}
		else
		{
			//if (character->_HP <= 0)
			//{
			//	++iter;
			//	DisconnectSession(session);

			//	continue;
			//}
			///* ***************************************
			//heart beat 체크
			//**************************************** */
			//if (cur - session->_last_packet_time > HEART_BEAT_LIMIT)
			//{
			//	LOG(LOG_LEVEL_SYSTEM, L"Heart beat fail.. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

			//	++iter;
			//	DisconnectSession(session);

			//	continue;
			//}

			MoveCharacter(character, session, dif_time);
			ChangeSector(character);
		}

		++iter;
	}
}

bool FrameSkip()
{
	/* ***************************************
	초당 프레임 체크
	**************************************** */
	static unsigned long FPS_check_start = timeGetTime();
	unsigned long FPS_check_end = timeGetTime();
	
	/* ***************************************
	프레임 스킵 체크
	**************************************** */
	static unsigned int prev_frame_time = timeGetTime();
	static int accumulate_frame_time = 0;
	static int frame = 0;
	static int loop = 0;
	static unsigned int max_dif_frame_second = 0;

	unsigned int current_frame_time = timeGetTime();
	const int SPF = SECOND_PER_FRAME; 
		
	bool b_skip = false;

	do
	{
		/* ***************************************
		바로 이전 시간과 현재 시간 사이에 상관 없이.
		프레임이 밀려 있다면 액션을 실행한다.
		(프레임이 밀린 경우 따라잡아야 하므로)
		**************************************** */
		if (accumulate_frame_time >= SPF)
		{
			accumulate_frame_time -= SPF;
			frame++;
			prev_frame_time = current_frame_time;
			
			break;
		}

		/* ***************************************
		바로 이전 콜과 현재 콜 사이의 시간이 SPF이상이면,
		액션 호출.
		**************************************** */
		if (current_frame_time - prev_frame_time >= SPF)
		{
			unsigned int dif_time = current_frame_time - prev_frame_time;
			
			max_dif_frame_second = max(max_dif_frame_second, (dif_time));
			accumulate_frame_time += (dif_time) - SPF;

			frame++;
			prev_frame_time = current_frame_time;
		}
		else
		{
			b_skip = true;
		}

	} while (0);

	loop++;

	if (FPS_check_end - FPS_check_start >= 1000)
	{
		if (frame != FRAME_PER_SECOND)
		{
			wprintf(L"frame : %d, loop : %d max_dif_frame_time : %d, accumulated_time : %d \n", frame, loop, max_dif_frame_second, accumulate_frame_time);
			LOG(LOG_LEVEL_WARNING, L"frame : %d, loop : %d, max_dif_frame_time : %d, accumulated_time : %d", frame, loop, max_dif_frame_second, accumulate_frame_time);
		}

		frame = 0;
		loop = 0;
		max_dif_frame_second = 0;
		FPS_check_start = timeGetTime();
	}

	return b_skip;
}

void MoveCharacter(Character* const character, Session* const session, int dif_time)
{
	unsigned char action = character->_action;
	
	/* ***************************************
	NOTE: HP가 0 이하인 캐릭터들은 마지막 액션이
	반복된다.(안끊어주면). 따라서 스탠드로 바꿔줘야하는데
	이 부분을 여기서 하지말고 다른 곳에서 할 수도 있을듯.
	**************************************** */
	if (character->_HP <= 0)
	{
		action = ACTION_STAND;
	}
	
	/* ***************************************
	이동 거리 = 프레임당 이동 거리 * 프레임 수
	**************************************** */
	short move_distance_x = MOVE_DISTANCE_X;
	short move_distance_y = MOVE_DISTANCE_Y;
	
	switch (action)
	{
	case ACTION_MOVE_LL:
		if (character->_pos_x - move_distance_x > MAP_MOVE_LEFT)
		{
			character->_pos_x -= move_distance_x;
		}

		break;

	case ACTION_MOVE_LU:
		if (character->_pos_x - move_distance_x > MAP_MOVE_LEFT && character->_pos_y - move_distance_y > MAP_MOVE_TOP)
		{
			character->_pos_x -= move_distance_x;
			character->_pos_y -= move_distance_y;
		}

		break;

	case ACTION_MOVE_LD:
		if (character->_pos_x - move_distance_x > MAP_MOVE_LEFT && character->_pos_y + move_distance_y < MAP_MOVE_BOTTOM)
		{
			character->_pos_x -= move_distance_x;
			character->_pos_y += move_distance_y;
		}

		break;

	case ACTION_MOVE_RR:
		if (character->_pos_x + move_distance_x < MAP_MOVE_RIGHT)
		{
			character->_pos_x += move_distance_x;
		}

		break;

	case ACTION_MOVE_RU:
		if (character->_pos_x + move_distance_x < MAP_MOVE_RIGHT && character->_pos_y - move_distance_y > MAP_MOVE_TOP)
		{
			character->_pos_x += move_distance_x;
			character->_pos_y -= move_distance_y;
		}

		break;

	case ACTION_MOVE_RD:
		if (character->_pos_x + move_distance_x < MAP_MOVE_RIGHT && character->_pos_y + move_distance_y < MAP_MOVE_BOTTOM)
		{
			character->_pos_x += move_distance_x;
			character->_pos_y += move_distance_y;
		}

		break;

	case ACTION_MOVE_UU:
		if (character->_pos_y - move_distance_y > MAP_MOVE_TOP)
		{
			character->_pos_y -= move_distance_y;
		}

		break;

	case ACTION_MOVE_DD:
		if (character->_pos_y + move_distance_y < MAP_MOVE_BOTTOM)
		{
			character->_pos_y += move_distance_y;
		}

		break;

	case ACTION_STAND:

		break;

	default:
		LOG(LOG_LEVEL_ERROR, L"Wrong Action type. type : %d, socket : %I64u, ID : %d", action, session->_socket, session->_ID);
		MakeSessionDisconnectThisTime(session);
		break;
	}
}

void ChangeSector(Character* const character)
{
	int old_sector_y = character->_sector_y;
	int old_sector_x = character->_sector_x;

	if (LeaveSector(character) && EnterSector(character))
	{
		int cur_sector_y = character->_sector_y;
		int cur_sector_x = character->_sector_x;

		MulticastLeaveSector(character->_ID, old_sector_x, old_sector_y, cur_sector_x, cur_sector_y);
		MulticastEnterSector(character->_ID, old_sector_x, old_sector_y, cur_sector_x, cur_sector_y);
	}
}
