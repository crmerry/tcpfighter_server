#include "stdafx.h"
#include "SerializationBuffer.h"

SerializationBuffer::SerializationBuffer(int size)
	:_buffer(nullptr), _size(size), _front(0), _rear(0)
{
	_buffer = new char[size];
}

SerializationBuffer::~SerializationBuffer()
{
	delete[] _buffer;
}

void SerializationBuffer::Release()
{
	delete[] _buffer;
}

void SerializationBuffer::Clear()
{
	_front = 0;
	_rear = 0;
}

int SerializationBuffer::GetBufferSize() const
{
	return _size;
}

int SerializationBuffer::GetDataSize() const
{
	return _rear - _front;
}

char* SerializationBuffer::GetBufferPtr() const
{
	return _buffer;
}

int SerializationBuffer::MoveWritePos(int size)
{
	if (_rear + size < _size)
	{
		_rear += size;

		return size;
	}
	else
	{
		int write_size = _size - _rear;
		_rear = _size;
		return write_size;
	}
}

int SerializationBuffer::MoveReadPos(int size)
{
	if (_front == _rear)
	{
		return 0;
	}
	else if (_front + size <= _rear)
	{
		_front += size;

		return size;
	}
	else if (_front + size > _rear)
	{
		int read_size = _rear - _front;
		_front = _rear;

		return read_size;
	}
	else
	{
		return 0;
	}
}

int SerializationBuffer::GetData(char* dest, int size)
{
	if (_front == _rear)
	{
		return 0;
	}

	if (_front + size <= _rear)
	{
		memcpy_s(dest, size, &_buffer[_front], size);
		_front += size;

		return size;
	}
	else
	{
		int read_size = _rear - _front;
		memcpy_s(dest, size, &_buffer[_front], read_size);
		_front += read_size;

		return read_size;
	}
}

int SerializationBuffer::PutData(char* src, int size)
{
	if (_rear + size >= _size)
	{
		char* reallocated_buf = new char[_size * 2];

		memcpy_s(reallocated_buf, _size * 2, _buffer, GetDataSize());
		_size = _size * 2;
		delete[] _buffer;
		_buffer = reallocated_buf;
	}

	memcpy_s(&_buffer[_rear], size, src, size);
	_rear += size;

	return size;
}

SerializationBuffer& SerializationBuffer::operator=(const SerializationBuffer& copy)
{
	if (_buffer == copy.GetBufferPtr())
	{
		return *this;
	}

	_size = copy.GetBufferSize();
	int data_size = copy.GetDataSize();

	if (_buffer != nullptr)
	{
		delete[] _buffer;
	}

	_buffer = new char[_size];
	memcpy_s(_buffer, _size, copy.GetBufferPtr(), data_size);
	_front = 0;
	_rear = data_size;

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(char value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(unsigned char value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(short value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(unsigned short value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(int value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(unsigned int value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer & SerializationBuffer::operator<<(long value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer & SerializationBuffer::operator<<(unsigned long value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(float value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(double value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(__int64 value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer & SerializationBuffer::operator<<(unsigned __int64 value)
{
	PutData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator<<(wchar_t* value)
{
	short len = (short)wcslen(value);
	*this << len;
	
	PutData((char*)value, len * sizeof(wchar_t));

	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (char& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (unsigned char& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (short& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (unsigned short& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}
SerializationBuffer& SerializationBuffer::operator >> (int& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (unsigned int& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}
SerializationBuffer& SerializationBuffer::operator >> (float& value)
{
	GetData((char*)&value, sizeof value);
	
	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (double& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator >> (__int64& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer & SerializationBuffer::operator>>(unsigned __int64& value)
{
	GetData((char*)&value, sizeof value);

	return *this;
}

SerializationBuffer& SerializationBuffer::operator>>(wchar_t* value)
{
	short len;
	*this >> len;
	
	GetData((char*)value, len * sizeof(wchar_t));
	
	return *this;
}
