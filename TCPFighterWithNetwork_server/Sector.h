#pragma once

#include "stdafx.h"
#include "Contents.h"
#include "Session.h"
#include "Character.h"
#include "Sector.h"

/* ***************************************
구조체
**************************************** */
struct Sector
{
	short _x;
	short _y;
};

struct TargetSector
{
	int		_size;
	Sector	_sector[9];
};

struct SetOfTargetSector
{
	TargetSector _set_of_sectors[9];
};

/* ***************************************
외부 전역 변수
**************************************** */
extern std::map <SOCKET, Session*> g_session_map;

extern std::map <int, Character*> g_character_map;

extern std::list<int> g_sector_arr[SECTOR_MAX_Y + 1][SECTOR_MAX_X + 1];

/* ***************************************
함수
**************************************** */
bool EnterSector(Character* const character);
bool LeaveSector(Character* const character);

void SetSector(Character* const character);
void RemoveCharacterInSector(Character* const character);
bool IsSameSector(Character* const character);
bool CheckSectorOutOfRange(int index);
void InformSessionOfNeighborsDeleted(int ID, TargetSector* spread_sector);
void InformNeighborsOfCharacter(int ID, TargetSector* spread_sector);

void MulticastLeaveSector(int ID, short old_x, short old_y, short cur_x, short cur_y);
void MulticastEnterSector(int ID, short old_x, short old_y, short cur_x, short cur_y);

void InformLeavingSector(int ID, TargetSector* target_sector);
void CalculateNeighborsOfSector(TargetSector* target_sector, short cur_x, short cur_y);

void CalculateLeaveLeftUpSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveUpSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveRightUpSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveLeftSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveCurrentSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveRightSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveLeftDownSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveDownSector(TargetSector* target_sector, short old_x, short old_y);
void CalculateLeaveRightDownSector(TargetSector* target_sector, short old_x, short old_y);

void InformCharacterOfNeighbors(int ID, TargetSector* target_sector);
void CalculateEnterLeftUpSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterUpSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterRightUpSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterLeftSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterCurrentSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterRightSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterLeftDownSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterDownSector(TargetSector * target_sector, short cur_x, short cur_y);
void CalculateEnterRightDownSector(TargetSector * target_sector, short cur_x, short cur_y);
