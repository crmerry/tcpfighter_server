#pragma once

/////////////////////////////////////////////////////////////////////
// www.gamecodi.com						이주행 master@gamecodi.com
//
//
/////////////////////////////////////////////////////////////////////
/*---------------------------------------------------------------

패킷데이터 정의.


자신의 캐릭터에 대한 패킷을 서버에게 보낼 때, 모두 자신이 먼저
액션을 취함과 동시에 패킷을 서버로 보내주도록 한다.

- 이동 키 입력 시 이동동작을 취함과 동시에 이동 패킷을 보내도록 한다.
- 공격키 입력 시 공격 동작을 취하면서 패킷을 보낸다.
- 충돌 처리 및 데미지에 대한 정보는 서버에서 처리 후 통보하게 된다.

---------------------------------------------------------------*/
#define SERVER_PORT			(20000)
#define SERVER_IP_ANY		L"0.0.0.0"
#define PACKET_MAX_SIZE		(50)

#define TCP_KEEP_ALIVE_TIME		(50000)
#define TCP_KEEP_ALIVE_INTERVAL (30000)
//---------------------------------------------------------------
// 패킷헤더.
//
//---------------------------------------------------------------
/*
	BYTE	byCode;			// 패킷코드 0x89 고정.
	BYTE	bySize;			// 패킷 사이즈.  (실제 페이로드 사이즈, 헤더/End 코드 제외)
	BYTE	byType;			// 패킷타입.
	BYTE	byTemp;			// 사용안함.
*/
#pragma pack(push, 1)
struct PacketHeader
{
	unsigned char	_code;			// 패킷코드 0x89 고정.
	unsigned char	_size;			// 패킷 사이즈.
	unsigned char	_type;			// 패킷타입.
	unsigned char	_temp;			// 사용안함.
};
#pragma pack(pop)
//---------------------------------------------------------------
// 패킷의 가장 앞에 들어갈 패킷코드.
//---------------------------------------------------------------
#define PACKET_HEADER_CODE	((BYTE)0x89)
//---------------------------------------------------------------
// 패킷의 가장 뒤에 들어갈 패킷코드.
//---------------------------------------------------------------
#define PACKET_END_CODE	((BYTE)0x79)
//---------------------------------------------------------------
// 패킷의 끝 부분에는 1Byte 의 EndCode 가 포함된다.  0x79
//
//---------------------------------------------------------------
#define PACKET_END_CODE_SIZE (1)

#define	PACKET_CREATE_MY_CHARACTER_SC	(0)
//---------------------------------------------------------------
// 0 - 클라이언트 자신의 캐릭터 할당		Server -> Client
//
// 서버에 접속시 최초로 받게되는 패킷으로 자신이 할당받은 ID 와
// 자신의 최초 위치, HP 를 받게 된다. (처음에 한번 받게 됨)
// 
// 이 패킷을 받으면 자신의 ID,X,Y,HP 를 저장하고 캐릭터를 생성시켜야 한다.
//
//	4	-	ID
//	1	-	Direction
//	2	-	X
//	2	-	Y
//	1	-	HP
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketCreateMyCharacterSC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
	unsigned char	_HP;
};
#pragma pack(pop)
#define	PACKET_CREATE_OTHER_CHARACTER_SC	(1)
//---------------------------------------------------------------
// 1. 다른 클라이언트의 캐릭터 생성 패킷		Server -> Client
//
// 처음 서버에 접속시 이미 접속되어 있던 캐릭터들의 정보
// 또는 게임중에 접속된 클라이언트들의 생성 용 정보.
//
//
//	4	-	ID
//	1	-	Direction
//	2	-	X
//	2	-	Y
//	1	-	HP
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketCreateOtherCharacterSC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
	unsigned char	_HP;
};
#pragma pack(pop)

#define	PACKET_DELETE_CHARACTER_SC			(2)
//---------------------------------------------------------------
// 2. 캐릭터 삭제 패킷						Server -> Client
//
// 캐릭터의 접속해제 또는 캐릭터가 죽었을때 전송됨.
//
//	4	-	ID
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketDeleteCharacter
{
	int	_ID;
};
#pragma pack(pop)

#define	PACKET_MOVE_START_CS				(10)
//---------------------------------------------------------------
// 10. 캐릭터 이동시작 패킷						Client -> Server
//
// 자신의 캐릭터 이동시작시 이 패킷을 보낸다.
// 이동 중에는 본 패킷을 보내지 않으며, 키 입력이 변경되었을 경우에만
// 보내줘야 한다.
//
// (왼쪽 이동중 위로 이동 / 왼쪽 이동중 왼쪽 위로 이동... 등등)
//
//	1	-	Direction	( 방향 디파인 값 8방향 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#define PACKET_MOVE_DIR_LL					(0)
#define PACKET_MOVE_DIR_LU					(1)
#define PACKET_MOVE_DIR_UU					(2)
#define PACKET_MOVE_DIR_RU					(3)
#define PACKET_MOVE_DIR_RR					(4)
#define PACKET_MOVE_DIR_RD					(5)
#define PACKET_MOVE_DIR_DD					(6)
#define PACKET_MOVE_DIR_LD					(7)
#pragma pack(push, 1)
struct PacketMoveStartCS
{
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_MOVE_START_SC					(11)
//---------------------------------------------------------------
// 캐릭터 이동시작 패킷						Server -> Client
//
// 다른 유저의 캐릭터 이동시 본 패킷을 받는다.
// 패킷 수신시 해당 캐릭터를 찾아 이동처리를 해주도록 한다.
// 
// 패킷 수신 시 해당 키가 계속해서 눌린것으로 생각하고
// 해당 방향으로 계속 이동을 하고 있어야만 한다.
//
//	4	-	ID
//	1	-	Direction	( 방향 디파인 값 8방향 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketMoveStartSC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_MOVE_STOP_CS					(12)
//---------------------------------------------------------------
// 캐릭터 이동중지 패킷						Client -> Server
//
// 이동중 키보드 입력이 없어서 정지되었을 때, 이 패킷을 서버에 보내준다.
//
//	1	-	Direction	( 방향 디파인 값 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketMoveStopCS
{
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_MOVE_STOP_SC					(13)
//---------------------------------------------------------------
// 캐릭터 이동중지 패킷						Server -> Client
//
// ID 에 해당하는 캐릭터가 이동을 멈춘것이므로 
// 캐릭터를 찾아서 방향과, 좌표를 입력해주고 멈추도록 처리한다.
//
//	4	-	ID
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketMoveStopSC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)


#define	PACKET_ATTACK1_CS						(20)
//---------------------------------------------------------------
// 캐릭터 공격 패킷							Client -> Server
//
// 공격 키 입력시 본 패킷을 서버에게 보낸다.
// 충돌 및 데미지에 대한 결과는 서버에서 알려 줄 것이다.
//
// 공격 동작 시작시 한번만 서버에게 보내줘야 한다.
//
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y	
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketAttack1CS
{
	unsigned char _direction;
	unsigned short _x;
	unsigned short _y;
};
#pragma pack(pop)

#define	PACKET_ATTACK1_SC						(21)
//---------------------------------------------------------------
// 캐릭터 공격 패킷							Server -> Client
//
// 패킷 수신시 해당 캐릭터를 찾아서 공격1번 동작으로 액션을 취해준다.
// 방향이 다를 경우에는 해당 방향으로 바꾼 후 해준다.
//
//	4	-	ID
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketAttack1SC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)


#define	PACKET_ATTACK2_CS						(22)
//---------------------------------------------------------------
// 캐릭터 공격 패킷							Client -> Server
//
// 공격 키 입력시 본 패킷을 서버에게 보낸다.
// 충돌 및 데미지에 대한 결과는 서버에서 알려 줄 것이다.
//
// 공격 동작 시작시 한번만 서버에게 보내줘야 한다.
//
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketAttack2CS
{
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_ATTACK2_SC						(23)
//---------------------------------------------------------------
// 캐릭터 공격 패킷							Server -> Client
//
// 패킷 수신시 해당 캐릭터를 찾아서 공격2번 동작으로 액션을 취해준다.
// 방향이 다를 경우에는 해당 방향으로 바꾼 후 해준다.
//
//	4	-	ID
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketAttack2SC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_ATTACK3_CS						(24)
//---------------------------------------------------------------
// 캐릭터 공격 패킷							Client -> Server
//
// 공격 키 입력시 본 패킷을 서버에게 보낸다.
// 충돌 및 데미지에 대한 결과는 서버에서 알려 줄 것이다.
//
// 공격 동작 시작시 한번만 서버에게 보내줘야 한다.
//
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketAttack3CS
{
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_ATTACK3_SC						(25)
//---------------------------------------------------------------
// 캐릭터 공격 패킷							Server -> Client
//
// 패킷 수신시 해당 캐릭터를 찾아서 공격3번 동작으로 액션을 취해준다.
// 방향이 다를 경우에는 해당 방향으로 바꾼 후 해준다.
//
//	4	-	ID
//	1	-	Direction	( 방향 디파인 값. 좌/우만 사용 )
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------
#pragma pack(push, 1)
struct PacketAttack3SC
{
	int				_ID;
	unsigned char	_direction;
	unsigned short	_x;
	unsigned short	_y;
};
#pragma pack(pop)

#define	PACKET_DAMAGE_SC						(30)
//---------------------------------------------------------------
// 캐릭터 데미지 패킷							Server -> Client
//
// 공격에 맞은 캐릭터의 정보를 보냄.
//
//	4	-	AttackID	( 공격자 ID )
//	4	-	DamageID	( 피해자 ID )
//	1	-	DamageHP	( 피해자 HP )
//
//---------------------------------------------------------------
struct PacketDamageSC
{
	int		_attack_ID;
	int		_damage_ID;
	unsigned char _HP;
};



#define	PACKET_SC_SYNC							(251)
//---------------------------------------------------------------
// 동기화를 위한 패킷					Server -> Client
//
// 서버로부터 동기화 패킷을 받으면 해당 캐릭터를 찾아서
// 캐릭터 좌표를 보정해준다.
//
//	4	-	ID
//	2	-	X
//	2	-	Y
//
//---------------------------------------------------------------

#define PACKET_ECHO_CS							(252)
/* ***************************************
4	- TIME
**************************************** */

#define PACKET_ECHO_SC							(253)
/* ***************************************
4 - TIME.
**************************************** */

#define PACKET_NOT_USED							(999)
