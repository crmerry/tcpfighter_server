#pragma once

struct Session;

/* ***************************************
구조체
**************************************** */
struct Character
{
	int				_ID;
	Session*		_session;

	unsigned char	_action;
	unsigned char	_direction;

	short			_pos_x;
	short			_pos_y;
	short			_action_x;
	short			_action_y;

	short			_sector_x;
	short			_sector_y;

	char			_HP;
};

/* ***************************************
함수
**************************************** */
void InitializeCharacter(Character* const character, Session* const session);

bool CreateCharacter(Character** out_character, Session* const session);
void DeleteCharacter(Character* const character);

bool AddCharacter(Character* const character);
bool RemoveCharacter(Character* const character);

bool FindCharacter(Character** Character, int ID);

void SetCharacterPosition(short x, short y, Character* const character);
void SetCharacterActionPosition(short x, short y, Character* const character);
bool IsCharacterMove(Character* const character);
void SetDirection(unsigned char direction, Character* const character);