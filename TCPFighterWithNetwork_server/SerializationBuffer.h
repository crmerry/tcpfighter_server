#ifndef  __SERIALIZATION_BUFFER__
#define  __SERIALIZATION_BUFFER__

class SerializationBuffer
{
public:
	enum eSerializationBuffer
	{
		BUFFER_DEFAULT = 2048
	};
	SerializationBuffer(int size = eSerializationBuffer::BUFFER_DEFAULT);
	virtual	~SerializationBuffer();

	void	Release();
	void	Clear();
	int		GetBufferSize() const;
	int		GetDataSize() const;
	char*	GetBufferPtr() const;
	int		MoveWritePos(int size);
	int		MoveReadPos(int size);
	int		GetData(char* dest, int size);
	int		PutData(char* src, int size);

	SerializationBuffer& operator =	 (const SerializationBuffer& copy);

	SerializationBuffer& operator << (char					value);
	SerializationBuffer& operator << (unsigned char			value);
	SerializationBuffer& operator << (short					value);
	SerializationBuffer& operator << (unsigned short		value);
	SerializationBuffer& operator << (int					value);
	SerializationBuffer& operator << (unsigned int			value);
	SerializationBuffer& operator << (long					value);
	SerializationBuffer& operator << (unsigned long			value);
	SerializationBuffer& operator << (float					value);
	SerializationBuffer& operator << (double				value);
	SerializationBuffer& operator << (__int64				value);
	SerializationBuffer& operator << (unsigned __int64		value);
	SerializationBuffer& operator << (wchar_t*				value);
	
	SerializationBuffer& operator >> (char&					value);
	SerializationBuffer& operator >> (unsigned char&		value);
	SerializationBuffer& operator >> (short&				value);
	SerializationBuffer& operator >> (unsigned short&		value);
	SerializationBuffer& operator >> (int&					value);
	SerializationBuffer& operator >> (unsigned int&			value);
	SerializationBuffer& operator >> (float&				value);
	SerializationBuffer& operator >> (double&				value);
	SerializationBuffer& operator >> (__int64&				value);
	SerializationBuffer& operator >> (unsigned __int64&		value);
	SerializationBuffer& operator >> (wchar_t*				value);
protected:
	char* _buffer;
	int _size;
	int _front;
	int _rear;
};

#endif