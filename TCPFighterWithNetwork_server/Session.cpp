#include "stdafx.h"
#include "Protocol.h"
#include "Contents.h"
#include "Log.h"
#include "RingBuffer.h"
#include "SerializationBuffer.h"
#include "Character.h"
#include "Sector.h"
#include "NetworkProcedure.h"
#include "Session.h"

extern std::map <SOCKET, Session*> g_session_map;
extern SOCKET g_listen_socket;
extern int g_ID;

extern void TerminateServer();

void InitializeSession(Session* const session)
{
	session->_recv_Q = new RingBuffer(RECV_QUEUE_SIZE);
	session->_send_Q = new RingBuffer(SEND_QUEUE_SIZE);
	session->_last_packet_time = timeGetTime();
	session->_last_attack_time = 0;
	session->_ID = g_ID++;
	session->_b_right_disconnect = false;
	session->_b_to_be_disconnect = false;
}

bool CreateSession(Session** out_session)
{
	Session* session = new Session();
	int address_len = sizeof session->_address;

	do
	{
		session->_socket = accept(g_listen_socket, (SOCKADDR*)&session->_address, &address_len);

		if (session->_socket == INVALID_SOCKET)
		{
			int error = WSAGetLastError();

			/* ***************************************
			NOTE: select 모델에서 listen socket이 활성화 되어, accept를 진행하는
			경우에는 이론적으로 WSAEWOULDBLOCK이 뜰 수 없다.
			다만, 논블락 소켓으로 바꾸면 해당 error가 활성화 되므로 체크해준다.
			그런데, WOULDBLOCK이 뜬 경우 딱히 해줄 건 없고, 그대로 break해주면 됨.
			**************************************** */
			if (error == WSAEWOULDBLOCK)
			{
				break;
			}

			if (error != WSAEWOULDBLOCK)
			{
				LOG(LOG_LEVEL_ERROR, L"Accept error & code : %d ", error);

				break;
			}
		}

		InitializeSession(session);

		if (!AddSession(session))
		{
			LOG(LOG_LEVEL_DEBUG, L"add session error. socket : %I64u, session ID : %d, map_size : %I64u", session->_socket, session->_ID, g_session_map.size());
			
			break;
		}

		wchar_t IP[16];
		InetNtop(AF_INET, (SOCKADDR*)&session->_address.sin_addr, IP, sizeof IP);
		
		LOG(LOG_LEVEL_SYSTEM, L"Connect. IP : %s, socket : %I64u, session ID : %d, map_size : %I64u", IP, session->_socket, session->_ID, g_session_map.size());
		
		*out_session = session;

		return true;

	} while (0);
	
	DeleteSession(session);

	return false;
}

void DeleteSession(Session* const session)
{
	if (session->_socket != INVALID_SOCKET)
	{
		closesocket(session->_socket);
	}

	delete session->_recv_Q;
	delete session->_send_Q;
	delete session;
}

bool AddSession(Session* const session)
{
	auto iter = g_session_map.insert(std::pair<SOCKET, Session*>(session->_socket, session));

	return iter.second;
}

bool RemoveSession(Session* const session)
{
	size_t remove_count = g_session_map.erase(session->_socket);

	return (remove_count == 1);
}

bool FindSession(Session** session, SOCKET socket)
{
	auto iter = g_session_map.find(socket);

	if (iter != g_session_map.end())
	{
		*session = iter->second;

		return true;
	}
	else
	{
		*session = nullptr;

		return false;
	}
}

/* ***************************************
세션을 바로 끊어줄 수도 있고, 한 번 늦게 끊어줄 수도 있음.
늦게 끊어 주는 경우, 송신 큐에 있는 남은 패킷을 보내주기 위해.
**************************************** */
void MakeSessionDisconnectThisTime(Session* const session)
{
	session->_b_right_disconnect = true;
}

void MakeSessionDisconnectNextTime(Session* const session)
{
	session->_b_to_be_disconnect = true;
}

/* ***************************************
실제 세션을 끊어주는 함수.
1. 주변 섹터에 캐릭터 삭제 패킷 전송.
2. 섹터 맵에서 해당 ID 삭제
3. 캐릭터 맵에서 해당 캐릭터 삭제
4. 세션 맵에서 해당 ID 삭제
**************************************** */
void DisconnectSession(Session* const session)
{
	wchar_t IP[16];
	InetNtop(AF_INET, (SOCKADDR*)&session->_address.sin_addr, IP, sizeof IP);

	LOG(LOG_LEVEL_SYSTEM, L"Disconnect. IP : %ls, SOCKET : %I64u, session_ID : %d", IP, session->_socket, session->_ID);
	
	/* ***************************************
	ID에 맞는 캐릭터 검색.
	**************************************** */
	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);
		
		return;
	}

	/* ***************************************
	1. 섹터 계산.
	**************************************** */
	TargetSector target_sector;

	CalculateNeighborsOfSector(&target_sector, character->_sector_x, character->_sector_y);
	
	/* ***************************************
	1. 삭제 패킷 생성
	**************************************** */
	SerializationBuffer packet;
	
	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 4, PACKET_DELETE_CHARACTER_SC);
	CreatePacketDeleteCharacterSC(&packet, character->_ID);

	/* ***************************************
	1. 섹터에 삭제 패킷 송신.
	**************************************** */
	SendPacketToMultiSector(&packet, &target_sector);

	/* ***************************************
	2.섹터맵에서 해당 ID 삭제. 3. 캐릭터 맵에서 해당 캐릭터 해제.
	**************************************** */
	RemoveCharacterInSector(character);
	DeleteCharacter(character);
	
	/* ***************************************
	4.세션 해제 & 세션 맵에서 삭제.
	**************************************** */
	if (!RemoveSession(session))
	{
		LOG(LOG_LEVEL_ERROR, L"Can not remove session. IP : %ls, SOCKET : %I64u, session_ID : %d, session_map_size : %I64u", IP, session->_socket, session->_ID, g_session_map.size());
	}

	DeleteSession(session);
}

void DisconnectAllSession()
{
	auto session_iter = g_session_map.begin();
	auto session_end = g_session_map.end();

	while (session_iter != session_end)
	{
		Session* session = session_iter->second;
		++session_iter;

		DisconnectSession(session);
	}
}

void UpdateRecvPacketTime(Session* const session)
{
	session->_last_packet_time = session->_current_packet_time;
	session->_current_packet_time = 0;
}