#pragma once
/* ***************************************
맵 사이즈
**************************************** */
#define MAP_MOVE_TOP		(0)
#define MAP_MOVE_LEFT		(0)
#define MAP_MOVE_RIGHT		(6400)
#define MAP_MOVE_BOTTOM		(6400)

/* ***************************************
각 이미지 사이즈
**************************************** */
#define TILE_SIZE			(64)
#define TILE_CENTER_X		(32)
#define TILE_CENTER_Y		(64)

#define CHARACTER_CENTER_X	(71)
#define CHARACTER_CENTER_Y	(93)
#define EFFECT_CENTER_X		(70)
#define EFFECT_CENTER_Y		(70)
#define HPGAUAGE_CENTER_X	(35)
#define HPGAUAGE_CENTER_Y	(7)
#define SHADOW_CENTER_X		(32)
#define SHADOW_CENTER_Y		(9)

/* ***************************************
프레임당 이동 거리 및 시간
**************************************** */
// 50 프레임 기준 x 3, y 2 이므로
// 25 프레임에선 x 6 y 4로 해야함.
// 10 프레임 기준 x 15, y 10
#define MOVE_DISTANCE_X		(6)
#define MOVE_DISTANCE_Y		(4)
#define FRAME_PER_SECOND	(25)
#define SECOND_PER_FRAME	(40)
#define MOVE_LIMIT_DISTANCE	(50)

/* ***************************************
액션 번호
**************************************** */
#define ACTION_MOVE_LL		(0)
#define ACTION_MOVE_LU		(1)
#define ACTION_MOVE_UU		(2)
#define ACTION_MOVE_RU		(3)
#define ACTION_MOVE_RR		(4)
#define ACTION_MOVE_RD		(5)
#define ACTION_MOVE_DD		(6)
#define ACTION_MOVE_LD		(7)

#define ACTION_ATTACK1		(11)
#define ACTION_ATTACK2		(12)
#define ACTION_ATTACK3		(13)

#define ACTION_STAND		(255)

/* ***************************************
액션 데미지
**************************************** */
#define ACTION_ATTACK1_DAMAGE		(5)
#define ACTION_ATTACK2_DAMAGE		(10)
#define ACTION_ATTACK3_DAMAGE		(15)

/* ***************************************
각 애니메이션당 딜레이 수치
**************************************** */
#define DELAY_STAND			(5)
#define DELAY_MOVE			(4)
#define DELAY_ATTACK1		(3)
#define DELAY_ATTACK2		(4)
#define DELAY_ATTACK3		(4)
#define DELAY_EFFECT		(3)

/* ***************************************
세션 관련 수치
**************************************** */
#define SESSION_COUNT_MAX	(6000)
#define SESSION_NOT_USED	(-1)

#define SEND_QUEUE_SIZE		(8192)
#define RECV_QUEUE_SIZE		(8192)

#define SELECT_SIZE			(FD_SETSIZE)

#define HEART_BEAT_LIMIT	(30000)

/* ***************************************
섹터 관련 수치
**************************************** */
#define SECTOR_SIZE			(400)
#define SECTOR_MAX_X		(15) 
#define SECTOR_MAX_Y		(15)
// 200, 31, 31
//MAP_MOVE_RIGHT / SECTOR_SIZE 는 총 16, 따라서 0~15 까지.
// 400, 15, 15

/* ***************************************
캐릭터 관련 수치
**************************************** */
#define INIT_POS_X			(0)	// 0~6100까지만
#define	INIT_POS_Y			(0)	// 0~6100까지만

