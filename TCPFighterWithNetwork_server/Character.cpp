#include "stdafx.h"
#include "Protocol.h"
#include "Contents.h"
#include "Session.h"
#include "Character.h"
#include "LOG.h"

extern std::map <int, Character*> g_character_map;

void InitializeCharacter(Character* const character, Session* const session)
{
	character->_ID = session->_ID;
	character->_session = session;

	character->_action = ACTION_STAND;
	character->_direction = ACTION_MOVE_RR;

	character->_pos_x = rand() % (MAP_MOVE_RIGHT-1) +1;
	character->_pos_y = rand() % (MAP_MOVE_BOTTOM- 1) + 1;

	character->_action_x = character->_pos_x;
	character->_action_y = character->_pos_y;

	character->_sector_x = character->_pos_x / SECTOR_SIZE;
	character->_sector_y = character->_pos_y / SECTOR_SIZE;

	character->_HP = 100;
}

bool CreateCharacter(Character** out_character, Session* const session)
{
	Character* character = new Character;

	InitializeCharacter(character, session);
	
	if (AddCharacter(character))
	{
		*out_character = character;

		LOG(LOG_LEVEL_DEBUG, L"ID : %d, x : %d, y : %d", character->_ID, character->_pos_x, character->_pos_y);
		return true;
	}

	return false;
}

/* ***************************************
세션은 여기서 지우지 않는다.
**************************************** */
void DeleteCharacter(Character* const character)
{
	g_character_map.erase(character->_ID);

	delete character;
}

bool AddCharacter(Character* const character)
{
	auto iter = g_character_map.insert(std::pair<int, Character*>(character->_ID, character));

	return iter.second;
}

bool RemoveCharacter(Character* const character)
{
	size_t remove_count = g_character_map.erase(character->_ID);

	return (remove_count == 1);
}

bool FindCharacter(Character** character, int ID)
{
	auto iter = g_character_map.find(ID);

	if (iter != g_character_map.end())
	{
		*character = iter->second;

		return true;
	}
	else
	{
		return false;
	}
}

void SetCharacterPosition(short x, short y, Character* const character)
{
	character->_pos_x = max(x, MAP_MOVE_LEFT + 1);
	character->_pos_x = min(x, MAP_MOVE_RIGHT -1);
	character->_pos_y = max(y, MAP_MOVE_TOP + 1);
	character->_pos_y = min(y, MAP_MOVE_BOTTOM -1);
}

void SetCharacterActionPosition(short x, short y, Character* const character)
{
	character->_action_x = x;
	character->_action_y = y;
}
bool IsCharacterMove(Character* const character)
{
	return (character->_action >= 0 && character->_action <= 7);
}

void SetDirection(unsigned char direction, Character* const character)
{
	if (direction == ACTION_MOVE_LD || direction == ACTION_MOVE_LL || direction == ACTION_MOVE_LU)
	{
		character->_direction = ACTION_MOVE_LL;
	}

	if (direction == ACTION_MOVE_RD || direction == ACTION_MOVE_RR || direction == ACTION_MOVE_RU)
	{
		character->_direction = ACTION_MOVE_RR;
	}
}
