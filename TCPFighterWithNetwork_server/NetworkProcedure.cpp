#include "stdafx.h"
#include "RingBuffer.h"
#include "SerializationBuffer.h"
#include "Protocol.h"
#include "Contents.h"
#include "Character.h"
#include "Session.h"
#include "Sector.h"
#include "NetworkProcedure.h"
#include "Log.h"

extern RingBuffer g_broadcast_Q;
extern std::list<BroadCastUnit> g_broadcast_except_list;

extern std::map <SOCKET, Session*> g_session_map;

extern std::map <int, Character*> g_character_map;

extern std::list<int> g_sector_arr[SECTOR_MAX_Y + 1][SECTOR_MAX_X + 1];

extern int g_sync_call;

extern void TerminateServer();
extern bool CompareServerAndClientPosition(unsigned short client_x, unsigned short client_y, Character* character);
extern bool CheckAttack(int* ID, Character* character, TargetSector* search_sector);
extern bool DeadReckoning(int* out_estimate_x, int* out_estimate_y, Character* const character);
extern bool ComparePosition(short pos_x1, short pos_y1, short pos_x2, short pos_y2);
extern void ChangeSector(Character* const character);
/* ***************************************
패킷 생성
**************************************** */
void CreatePacketHeader(SerializationBuffer* const packet, unsigned char code, unsigned char size, unsigned char type)
{
	*packet << code;
	*packet << size;
	*packet << type;
	*packet << (unsigned char)0; // temp 쓰지 않느다.
}

void CreatePacketCreateMyCharacterSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y, unsigned char HP)
{
	*packet << ID;
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << HP;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketCreateOtherCharacterSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y, unsigned char HP)
{
	*packet << ID;
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << HP;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketDeleteCharacterSC(SerializationBuffer* const packet, int ID)
{
	*packet << ID;
	*packet << (unsigned char)PACKET_END_CODE;
}

/* ***************************************
무브에서의 방향은, 캐릭터의 액션(이동)을 보내줘야함.
**************************************** */
void CreatePacketMoveStartSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << ID;
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketMoveStopSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << ID;
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack1SC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << ID;
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketDamageSC(SerializationBuffer* const packet, int attack_ID, int damage_ID, unsigned char damaga_HP)
{
	*packet << attack_ID;
	*packet << damage_ID;
	*packet << damaga_HP;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketSyncSC(SerializationBuffer* const packet, int ID, unsigned short x, unsigned short y)
{
	*packet << ID;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketEchoSC(SerializationBuffer* const packet, unsigned int time)
{
	*packet << time;
	*packet << (unsigned char)PACKET_END_CODE;
}

/* ***************************************
네트워크 프로시져
**************************************** */
void NetCreateMyCharacterSC(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 10, PACKET_CREATE_MY_CHARACTER_SC);
	CreatePacketCreateMyCharacterSC(&packet, character->_ID, character->_direction, character->_pos_x, character->_pos_y, character->_HP);

	SendPacketByUnicast(&packet, session);
}

void NetCreateOtherCharacterSC(Session* const session, int ID, unsigned char direction, unsigned short x, unsigned short y, unsigned char HP)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 10, PACKET_CREATE_OTHER_CHARACTER_SC);
	CreatePacketCreateOtherCharacterSC(&packet, ID, direction, x, y, HP);

	SendPacketByUnicast(&packet, session);
}

/* ***************************************
세션에 
1. 주변 섹터들의 캐릭터들을 생성하는 패킷
2. 액션이 있다면 액션 패킷
을 보내줌.
**************************************** */
void NetInformSessionOfNeighbors(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr. file : %s, line: %d", WFILE, __LINE__);

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}
	
	/* ***************************************
	섹터 계산
	**************************************** */
	short sector_x = character->_sector_x;
	short sector_y = character->_sector_y;

	TargetSector target_sector_info;

	CalculateNeighborsOfSector(&target_sector_info, sector_x, sector_y);

	for (int count = 0; count < target_sector_info._size; count++)
	{
		auto iter = g_sector_arr[target_sector_info._sector[count]._y][target_sector_info._sector[count]._x].begin();
		auto end = g_sector_arr[target_sector_info._sector[count]._y][target_sector_info._sector[count]._x].end();

		while (iter != end)
		{
			Character* around_character = nullptr;

			FindCharacter(&around_character, *iter);

			if (!around_character)
			{
				LOG(LOG_LEVEL_ERROR, L"Character is not exist. file : %s, line: %d, sector_x : %d, sector_y :%d", WFILE, __LINE__, sector_x, sector_y);

				TerminateServer();

				return;
			}

			/* ***************************************
			생성 및 액션 패킷 보내줌.
			**************************************** */
			NetCreateOtherCharacterSC(session, around_character->_ID, around_character->_direction, around_character->_pos_x, around_character->_pos_y, around_character->_HP);
			
			if (IsCharacterMove(around_character))
			{
				NetMoveStartSC(session, around_character);
			}

			++iter;
		}
	}
}

/* ***************************************
주변 섹터들에 속한 세션들에
1. 해당 세션의 캐릭터를 생성하는 패킷
2. 액션이 있다면 액션 패킷
을 보내줌.
**************************************** */
void NetInformNeighborsOfSession(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr. file : %s, line: %d", WFILE, __LINE__);

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 10, PACKET_CREATE_OTHER_CHARACTER_SC);
	CreatePacketCreateOtherCharacterSC(&packet, character->_ID, character->_direction, character->_pos_x, character->_pos_y, character->_HP);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet);

	if (IsCharacterMove(character))
	{
		SerializationBuffer packet;

		CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_MOVE_START_SC);
		CreatePacketMoveStartSC(&packet, character->_ID, character->_action, character->_pos_x, character->_pos_y);
		
		NetInformNeighborsOfSessionAboutThisPacket(session, &packet);
	}
}

void NetInformNeighborsOfSessionAboutThisPacket(Session* const session, SerializationBuffer* const packet, bool except_my_character)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr. file : %s, line: %d", WFILE, __LINE__);

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	short sector_x = character->_sector_x;
	short sector_y = character->_sector_y;

	TargetSector target_sector_info;

	CalculateNeighborsOfSector(&target_sector_info, sector_x, sector_y);

	if (except_my_character)
	{
		SendPacketToMultiSector(packet, &target_sector_info, character->_session);
	}
	else
	{
		SendPacketToMultiSector(packet, &target_sector_info);
	}
}

void NetDeleteCharacterInSector(Session* const session, int ID)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 4, PACKET_DELETE_CHARACTER_SC);
	CreatePacketDeleteCharacterSC(&packet, ID);

	SendPacketByUnicast(&packet, session);
}

void NetMoveStartCS(SerializationBuffer* const packet, Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	unsigned char direction;
	unsigned short x;
	unsigned short y;

	(*packet) >> direction;
	(*packet) >> x;
	(*packet) >> y;
	
	/* ***************************************
	디버깅용 로그
	**************************************** */
	{
		LOG(LOG_LEVEL_DEBUG, L"PACKET_MOVE_START_CS. ID : %d, direction : %d, x: %d, y: %d", session->_ID, direction, x, y);
#ifdef _DEBUG
		wchar_t debug_string[100];
		debug_string[99] = L'\0';
		swprintf_s(debug_string, 100, L"PACKET_MOVE_START_CS. ID : %d, direction : %d, x: %d, y: %d\n", session->_ID, direction, x, y);
		OutputDebugString(debug_string);
#endif // _DEBUG
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	if (CompareServerAndClientPosition(x, y, character))
	{
		SetCharacterPosition(x, y, character);
		SetCharacterActionPosition(x, y, character);
	}
	else
	{
		NetSyncPosBetweenClientAndServer(session, character, x, y, PACKET_MOVE_START_CS);
	}

	if (!IsSameSector(character))
	{
		ChangeSector(character);
	}

	SetDirection(direction, character);
	character->_action = direction;

	NetMoveStartSC(session);
}

void NetMoveStartSC(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_MOVE_START_SC);
	CreatePacketMoveStartSC(&packet, character->_ID, character->_action, character->_pos_x, character->_pos_y);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet);
}

void NetMoveStartSC(Session* const session, Character* const character)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_MOVE_START_SC);
	CreatePacketMoveStartSC(&packet, character->_ID, character->_action, character->_pos_x, character->_pos_y);

	SendPacketByUnicast(&packet, session);
}

void NetMoveStopCS(SerializationBuffer* const packet, Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	unsigned char direction;
	unsigned short x;
	unsigned short y;

	(*packet) >> direction;
	(*packet) >> x;
	(*packet) >> y;

	/* ***************************************
	디버깅용 로그
	**************************************** */
	{
		LOG(LOG_LEVEL_DEBUG, L"PACKET_MOVE_STOP_CS. ID : %d, direction : %d, x: %d, y: %d", session->_ID, direction, x, y);
#ifdef _DEBUG
		wchar_t debug_string[100];
		debug_string[99] = L'\0';
		swprintf_s(debug_string, 100, L"PACKET_MOVE_STOP_CS. ID : %d, direction : %d, x: %d, y: %d \n", session->_ID, direction, x, y);
		OutputDebugString(debug_string);
#endif // _DEBUG
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	if (CompareServerAndClientPosition(x, y, character))
	{
		SetCharacterPosition(x, y, character);
		SetCharacterActionPosition(x, y, character);
	}
	else
	{
		NetSyncPosBetweenClientAndServer(session, character, x, y, PACKET_MOVE_STOP_CS);
	}

	if (!IsSameSector(character))
	{
		ChangeSector(character);
	}

	SetDirection(direction, character);
	character->_action = ACTION_STAND;

	NetMoveStopSC(session);
}

void NetMoveStopSC(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_MOVE_STOP_SC);
	CreatePacketMoveStopSC(&packet, character->_ID, character->_direction, character->_pos_x, character->_pos_y);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet);
}

void NetAttack1CS(SerializationBuffer* const packet, Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	unsigned char direction;
	unsigned short x;
	unsigned short y;

	(*packet) >> direction;
	(*packet) >> x;
	(*packet) >> y;

	/* ***************************************
	디버깅용 로그
	**************************************** */
	{
		LOG(LOG_LEVEL_DEBUG, L"PACKET_ATTACK1_CS. ID : %d, direction : %d, x: %d, y: %d", session->_ID, direction, x, y);
#ifdef _DEBUG
		wchar_t debug_string[100];
		debug_string[99] = L'\0';
		swprintf_s(debug_string, 100, L"PACKET_ATTACK1_CS. ID : %d, direction : %d, x: %d, y: %d\n", session->_ID, direction, x, y);
		OutputDebugString(debug_string);
#endif // _DEBUG
	}

	Character* attack_character = nullptr;

	FindCharacter(&attack_character, session->_ID);

	if (!attack_character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		return;
	}

	if (session->_current_packet_time - session->_last_attack_time < 240)
	{
		return;
	}

	if (CompareServerAndClientPosition(x, y, attack_character))
	{
		SetCharacterPosition(x, y, attack_character);
		SetCharacterActionPosition(x, y, attack_character);
	}
	else
	{
		NetSyncPosBetweenClientAndServer(session, attack_character, x, y, PACKET_ATTACK1_CS);
	}

	if (!IsSameSector(attack_character))
	{
		ChangeSector(attack_character);
	}

	session->_last_attack_time = session->_current_packet_time;

	SetDirection(direction, attack_character);
	attack_character->_action = ACTION_STAND;

	NetAttack1SC(session);

	short sector_x = attack_character->_sector_x;
	short sector_y = attack_character->_sector_y;

	TargetSector target_sector_info;

	CalculateNeighborsOfSector(&target_sector_info, sector_x, sector_y);

	int ID = 0;

	CheckAttack(&ID, attack_character, &target_sector_info);

	if (!ID)
	{
		return;
	}
	
	Character* damage_character = nullptr;

	FindCharacter(&damage_character, ID);

	if (!damage_character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		return;
	}

	if ((char)damage_character->_HP - ACTION_ATTACK1_DAMAGE <= 0)
	{
		damage_character->_HP = 1;
	}
	else
	{
		damage_character->_HP -= ACTION_ATTACK1_DAMAGE;
	}

	NetDamageSC(session, damage_character->_session);
}

void NetAttack1SC(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_ATTACK1_SC);
	CreatePacketAttack1SC(&packet, character->_ID, character->_direction, character->_pos_x, character->_pos_y);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet);
}

void NetAttack2CS(SerializationBuffer* const packet, Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	unsigned char direction;
	unsigned short x;
	unsigned short y;

	(*packet) >> direction;
	(*packet) >> x;
	(*packet) >> y;

	/* ***************************************
	디버깅용 코드
	**************************************** */
	{
		LOG(LOG_LEVEL_DEBUG, L"PACKET_ATTACK2_CS. ID : %d, direction : %d, x: %d, y: %d", session->_ID, direction, x, y);
#ifdef _DEBUG
		wchar_t debug_string[100];
		debug_string[99] = L'\0';
		swprintf_s(debug_string, 100, L"PACKET_ATTAC2_CS. ID : %d, direction : %d, x: %d, y: %d \n", session->_ID, direction, x, y);
		OutputDebugString(debug_string);
#endif // _DEBUG
	}

	Character* attack_character = nullptr;

	FindCharacter(&attack_character, session->_ID);

	if (!attack_character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		return;
	}

	if (session->_current_packet_time - session->_last_attack_time < 240)
	{
		return;
	}

	if (CompareServerAndClientPosition(x, y, attack_character))
	{
		SetCharacterPosition(x, y, attack_character);
		SetCharacterActionPosition(x, y, attack_character);
	}
	else
	{
		NetSyncPosBetweenClientAndServer(session, attack_character, x, y, PACKET_ATTACK2_CS);
	}

	if (!IsSameSector(attack_character))
	{
		ChangeSector(attack_character);
	}
	
	session->_last_attack_time = session->_current_packet_time;

	SetDirection(direction, attack_character);
	attack_character->_action = ACTION_STAND;

	NetAttack2SC(session);

	short sector_x = attack_character->_sector_x;
	short sector_y = attack_character->_sector_y;

	TargetSector target_sector_info;

	CalculateNeighborsOfSector(&target_sector_info, sector_x, sector_y);

	int ID = 0;

	CheckAttack(&ID, attack_character, &target_sector_info);

	if (!ID)
	{
		return;
	}

	Character* damage_character = nullptr;

	FindCharacter(&damage_character, ID);

	if (!damage_character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		return;
	}

	if ((char)damage_character->_HP - ACTION_ATTACK2_DAMAGE <= 0)
	{
		damage_character->_HP = 1;
	}
	else
	{
		damage_character->_HP -= ACTION_ATTACK2_DAMAGE;
	}

	NetDamageSC(session, damage_character->_session);
}

void NetAttack2SC(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_ATTACK2_SC);
	CreatePacketAttack1SC(&packet, character->_ID, character->_direction, character->_pos_x, character->_pos_y);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet);
}

void NetAttack3CS(SerializationBuffer* const packet, Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	unsigned char direction;
	unsigned short x;
	unsigned short y;

	(*packet) >> direction;
	(*packet) >> x;
	(*packet) >> y;

	/* ***************************************
	디버깅용 로그
	**************************************** */
	{
	LOG(LOG_LEVEL_DEBUG, L"PACKET_ATTACK3_CS. ID : %d, direction : %d, x: %d, y: %d", session->_ID, direction, x, y);
#ifdef _DEBUG
	wchar_t debug_string[100];
	debug_string[99] = L'\0';
	swprintf_s(debug_string, 100, L"PACKET_ATTAC3_CS. ID : %d, direction : %d, x: %d, y: %d \n", session->_ID, direction, x, y);
	OutputDebugString(debug_string);
#endif // _DEBUG
	}

	Character* attack_character = nullptr;

	FindCharacter(&attack_character, session->_ID);

	if (!attack_character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		return;
	}

	if (session->_current_packet_time - session->_last_attack_time < 240)
	{
		return;
	}

	if (CompareServerAndClientPosition(x, y, attack_character))
	{
		SetCharacterPosition(x, y, attack_character);
		SetCharacterActionPosition(x, y, attack_character);
	}
	else
	{
		NetSyncPosBetweenClientAndServer(session, attack_character, x, y, PACKET_ATTACK3_CS);
	}

	if (!IsSameSector(attack_character))
	{
		ChangeSector(attack_character);
	}

	session->_last_attack_time = session->_current_packet_time;

	SetDirection(direction, attack_character);
	attack_character->_action = ACTION_STAND;

	NetAttack3SC(session);

	short sector_x = attack_character->_sector_x;
	short sector_y = attack_character->_sector_y;

	TargetSector target_sector_info;

	CalculateNeighborsOfSector(&target_sector_info, sector_x, sector_y);

	int ID = 0;

	CheckAttack(&ID, attack_character, &target_sector_info);

	if (!ID)
	{
		return;
	}

	Character* damage_character = nullptr;

	FindCharacter(&damage_character, ID);

	if (!damage_character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		return;
	}

	if ((char)damage_character->_HP - ACTION_ATTACK3_DAMAGE <= 0)
	{
		damage_character->_HP = 1;
	}
	else
	{
		damage_character->_HP -= ACTION_ATTACK3_DAMAGE;
	}

	
	NetDamageSC(session, damage_character->_session);
}

void NetAttack3SC(Session* const session)
{
	if (!session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, session->_socket, session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_ATTACK3_SC);
	CreatePacketAttack1SC(&packet, character->_ID, character->_direction, character->_pos_x, character->_pos_y);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet);
}

void NetDamageSC(Session* const attack_session, Session* const damage_session)
{
	if (!attack_session || !damage_session)
	{
		LOG(LOG_LEVEL_ERROR, L"Session nullptr.  file : %s, line : %d, session_map_size : %I64u", WFILE, __LINE__, g_session_map.size());

		TerminateServer();

		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, damage_session->_ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. file : %s, line: %d, socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", WFILE, __LINE__, damage_session->_socket, damage_session->_ID, g_session_map.size(), g_character_map.size());

		MakeSessionDisconnectThisTime(damage_session);

		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_DAMAGE_SC);
	CreatePacketDamageSC(&packet, attack_session->_ID, character->_ID, character->_HP);

	NetInformNeighborsOfSessionAboutThisPacket(damage_session, &packet, false);
}

void NetSyncSC(Session* const session, int ID, unsigned short x, unsigned short y, bool except_my_session)
{
	g_sync_call++;

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 8, PACKET_SC_SYNC);
	CreatePacketSyncSC(&packet, ID, x, y);

	NetInformNeighborsOfSessionAboutThisPacket(session, &packet, except_my_session);
}

void NetEchoCS(SerializationBuffer* const packet, Session* const session)
{
	unsigned int time;
	
	*packet >> time;

	SerializationBuffer res_packet;

	CreatePacketHeader(&res_packet, PACKET_HEADER_CODE, 4, PACKET_ECHO_SC);
	CreatePacketEchoSC(&res_packet, time);

	SendPacketByUnicast(&res_packet, session);
}

void NetSyncPosBetweenClientAndServer(Session* const session, Character* const character, unsigned short client_x, unsigned short client_y, unsigned char packet_type)
{
	int estimate_x;
	int estimate_y;

	DeadReckoning(&estimate_x, &estimate_y, character);

	if (ComparePosition(estimate_x, estimate_y, client_x, client_y))
	{
		/* ***************************************
		데드 레커닝 좌표와 클라이언트 좌표가 허용할만한 차이이므로
		클라이언트의 좌표를 수용. 즉, 서버가9 ~13이상 부족하거나 초과한것.
		**************************************** */
		SetCharacterPosition(client_x, client_y, character);
		SetCharacterActionPosition(client_x, client_y, character);
	}
	else
	{
		/* ***************************************
		클라의 좌표가 틀렸음.
		데드 레커닝 좌표로 동기화하고 뿌려줌.
		**************************************** */
		LOG(LOG_LEVEL_WARNING, L"SyncPosition. prev_action : %d, prev_x : %d, prev_y : %d\nprev_time : %d, current_time : %d, dif_time : %d \npacket_type : %d, ID : %d, direction : %d\nclient : (%d, %d), server : (%d, %d), estimate : (%d, %d)", character->_action, character->_action_x, character->_action_y, session->_last_packet_time, session->_current_packet_time, session->_current_packet_time - session->_last_packet_time, packet_type, character->_ID, character->_direction, client_x, client_y, character->_pos_x, character->_pos_y, estimate_x, estimate_y);
		/*LOG(LOG_LEVEL_WARNING, L"SyncPosition. prev_action : %d, prev_x : %d, prev_y : %d", character->_action, character->_action_x, character->_action_y);
		LOG(LOG_LEVEL_WARNING, L"SyncPosition. prev_time : %d, current_time : %d, dif_time : %d", session->_last_packet_time, session->_current_packet_time,session->_current_packet_time - session->_last_packet_time);
		LOG(LOG_LEVEL_WARNING, L"SyncPosition. packet_type : %d, ID : %d, direction : %d", packet_type, character->_ID, character->_direction);
		LOG(LOG_LEVEL_WARNING, L"SyncPosition(estimate). client : (%d, %d), server : (%d, %d), estimate : (%d, %d)", client_x, client_y, character->_pos_x, character->_pos_y, estimate_x, estimate_y);
		*/
		/* ***************************************
		씽크 패킷을 주변 섹터들에 있는 캐릭터에 보내줌(해당 캐릭터 포함)
		**************************************** */
		NetSyncSC(session, character->_ID, estimate_x, estimate_y, false);

		SetCharacterPosition(estimate_x, estimate_y, character);
		SetCharacterActionPosition(estimate_x, estimate_y, character);
	}

}

/* ***************************************
네트워크 수신부
**************************************** */
void RecvProc(Session* const session)
{
	SerializationBuffer packet;
	unsigned short type = PACKET_NOT_USED;

	bool b_recv = RecvPacket(session);
	bool b_complete_packet = false;

	while (b_recv)
	{
		if (!DecodePacket(&type, &packet, session))
		{
			break;
		}

		b_complete_packet = true;

		ProcPacket(type, &packet, session);

		packet.Clear();
	}

	if (b_complete_packet && type != PACKET_ECHO_CS)
	{
		UpdateRecvPacketTime(session);
	}
}

bool RecvPacket(Session* const session)
{
	int read_count = recv(session->_socket, session->_recv_Q->GetWriteBufferPtr(), session->_recv_Q->GetWritableSizeAtOnce(), 0);
	
	if (read_count > 0)
	{
		session->_recv_Q->MoveRearAfterWrite(read_count);

		return true;
	}
	else if (read_count == 0)
	{
		/* ***************************************
		FIN 이나 RST가 온것.
		즉 상대방에서 더 이상 보낼 것이 없다는 표시에
		의하여, 내 소켓도 recv에 0을 반환.
		**************************************** */
		MakeSessionDisconnectThisTime(session);

		return false;
	}
	else
	{
		int error = WSAGetLastError();

		/* ***************************************
		논블럭 소켓이므로 우드블럭 확인.
		**************************************** */
		if (error == WSAEWOULDBLOCK)
		{
			return false;
		}

		/* ***************************************
		접속 끊고 로그
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"recv error. error_code : %d, socket : %I64u, session_ID : %d", error, session->_socket, session->_ID);

		MakeSessionDisconnectThisTime(session);

		return false;
	}
}

bool DecodePacket(unsigned short* const type, SerializationBuffer* const packet, Session* const session)
{
	PacketHeader header;
	RingBuffer* recv_Q = session->_recv_Q;

	if (recv_Q->GetUsedSize() < sizeof(PacketHeader))
	{
		return false;
	}

	recv_Q->Peek((char*)&header, sizeof(PacketHeader));
	*type = header._type;

	if (header._code != PACKET_HEADER_CODE)
	{
		/* ***************************************
		패킷 코드가 다르다
		1 : 악성 패킷
		2 : 클라 혹은 서버 패킷 코드 문제
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Packet code error. socket : %I64u, session_ID : %d, packet_code : %d", session->_socket, session->_ID, header._code);
		
		MakeSessionDisconnectThisTime(session);

		return false;
	}

	int total_packet_size = sizeof(PacketHeader) + header._size + PACKET_END_CODE_SIZE;

	if (recv_Q->GetUsedSize() < total_packet_size)
	{
		return false;
	}

	recv_Q->MoveFrontAfterRead(sizeof(PacketHeader));

	recv_Q->Dequeue(packet->GetBufferPtr(), header._size);
	recv_Q->MoveFrontAfterRead(PACKET_END_CODE_SIZE);
	packet->MoveWritePos(header._size);

	/* ***************************************
	디코드 패킷에 성공해야, 패킷이 제대로 온것이므로
	패킷 받은 시간을 디코드 패킷에서 설정.
	**************************************** */
	session->_current_packet_time = timeGetTime();

	return true;
}

void ProcPacket(unsigned short type, SerializationBuffer* const packet, Session* const session)
{
	switch (type)
	{
	case PACKET_MOVE_START_CS:
		NetMoveStartCS(packet, session);
		break;

	case PACKET_MOVE_STOP_CS:
		NetMoveStopCS(packet, session);
		break;

	case PACKET_ATTACK1_CS:
		NetAttack1CS(packet, session);
		break;

	case PACKET_ATTACK2_CS:
		NetAttack2CS(packet, session);
		break;

	case PACKET_ATTACK3_CS:
		NetAttack3CS(packet, session);
		break;

	case PACKET_ECHO_CS:
		NetEchoCS(packet, session);
		break;

	default:
		/* ***************************************
		서버에서 패킷이 잘못 온 것
		or
		클라에서 패킷을 잘못 해석한것.
		접속종료처리 + 로그 처리
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Packet type error. socket : %I64u, session_ID : %d, packet_type : %d", session->_socket, session->_ID, type);

		MakeSessionDisconnectThisTime(session);

		break;
	}
}

/* ***************************************
네트워크 송신부
**************************************** */
bool SendProc(Session* const session)
{
	RingBuffer* send_Q = session->_send_Q;

	while (1)
	{
		if ((send_Q->GetUsedSize()) == 0)
		{
			return false;
		}

		int send_size = send(session->_socket, send_Q->GetReadBufferPtr(), send_Q->GetReadableSizeAtOnce(), 0);

		if (send_size == SOCKET_ERROR)
		{
			int error = WSAGetLastError();

			/* ***************************************
			논블럭 소켓이므로 우드블럭 체크
			**************************************** */
			if (error == WSAEWOULDBLOCK)
			{
				return true;
			}

			/* ***************************************
			접속 끊고 로그
			**************************************** */
			LOG(LOG_LEVEL_ERROR, L"Send proc error. error_coode : %d, socket : %I64u, session_ID : %d, ", error, session->_socket, session->_ID);

			MakeSessionDisconnectThisTime(session);

			return false;
		}

		send_Q->MoveFrontAfterRead(send_size);
	}
}

void SendPacketByUnicast(SerializationBuffer* const packet, Session* const session)
{
	int data_size = packet->GetDataSize();

	int enqueue_size = session->_send_Q->Enqueue(packet->GetBufferPtr(), data_size);

	if (enqueue_size != data_size)
	{
		/* ***************************************
		센드 큐에 들어가지 못했다는 것은.
		수신 측이 제대로 수신->처리를 못하고 있다는 것.
		혹은 서버에서 너무 많은 패킷을 보내고 있음.
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Can not enqueue data into send_Q. size : %d", session->_send_Q->GetBufferSize());
		
		MakeSessionDisconnectThisTime(session);
	}
}

/* ***************************************
브로드캐스트용 버퍼에 넣어준다. except_ID가 0이면 사용 x 1이상이면 해당 ID에 해당하는 세션은
제외됨.
**************************************** */
void SendPacketByBroadcast(SerializationBuffer* const packet, int except_ID)
{
	int data_size = packet->GetDataSize();

	int enqueue_size = g_broadcast_Q.Enqueue((char*)packet->GetBufferPtr(), data_size);
	g_broadcast_except_list.push_back(BroadCastUnit{ except_ID, data_size });

	if (enqueue_size != data_size)
	{
		// 브로드 캐스트용 링버퍼가 터진 상황.
		// 혹은 링버퍼 논리 오류.
		LOG(LOG_LEVEL_ERROR, L"Broadcast Q explosion. size : %d", g_broadcast_Q.GetBufferSize());

		TerminateServer();
	}
}

void SendPacketToOneSector(SerializationBuffer* const packet, short sector_x, short sector_y, Session* const except_session)
{
	auto iter = g_sector_arr[sector_y][sector_x].begin();
	auto end = g_sector_arr[sector_y][sector_x].end();

	while (iter != end)
	{
		Character* target = nullptr;

		FindCharacter(&target, *iter);

		if (!target)
		{
			LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d", WFILE, __LINE__);

			TerminateServer();

			return;
		}

		if (target->_session != except_session)
		{
			SendPacketByUnicast(packet, target->_session);
		}
		
		++iter;
	}
}

void SendPacketToMultiSector(SerializationBuffer* const packet, TargetSector* const target_sector, Session* const except_session)
{
	int size = target_sector->_size;

	if (size == 0)
	{
		return;
	}

	for (int count = 0; count < size; count++)
	{
		short x = target_sector->_sector[count]._x;
		short y = target_sector->_sector[count]._y;

		SendPacketToOneSector(packet, x, y, except_session);
	}
}

/* ***************************************
브로드 캐스트용 버퍼에 패킷이 있는 경우,
이 패킷을 다시 각 세션의 송신 버퍼에 넣어줌.

이 함수는 DistributeSessionsToSelect()안에서
브로드 캐스트 -> 셀렉트 콜의 순서가 반복적으로 이루어짐.
이 때, 셀렉트 콜의 결과로 브로드 캐스트용 큐에 다시 패킷이 추가될 수 있음.
이 때, 추가된 만큼 다시 보내는 것이 아니고,
DistributeSessionsToSelect() 함수 호출 초기에 브로드 캐스트용 버퍼에 있는 패킷 크기(packet_size)를 조사하여, 그 사이즈만큼만 보내준다.
그리고 DistributeSessionsToSelect()콜이 끝날 때,  packet_size만큼 땡겨준다.
그 결과, 셀렉트 콜 중에 생긴 브로드 캐스트용 패킷은 브로드 캐트스 버퍼에 남아있고,
다음 DistributeSessionsToSelect()에서 다시 보내주면 됨.
**************************************** */
void SendProcBroadcastPacketByDistribute(Session** session, size_t size, int packet_size, bool b_next)
{
	int readalbe_size_at_once = g_broadcast_Q.GetReadableSizeAtOnce();
	int except_ID = g_broadcast_except_list.front()._except_ID;

	if (readalbe_size_at_once >= packet_size)
	{
		for (size_t index = 0; index < size; index++)
		{
			if (session[index]->_ID == except_ID)
			{
				continue;
			}

			int enqueue_size = session[index]->_send_Q->Enqueue(g_broadcast_Q.GetReadBufferPtr(), packet_size);

			/* ***************************************
			세션의 센드 큐가 터진 상황. 끊어줌.
			**************************************** */
			if (enqueue_size != packet_size)
			{
				LOG(LOG_LEVEL_WARNING, L"Disconnect session by broadcast. socket : %I64u session_ID : %d size of packets : %d ", session[index]->_socket, session[index]->_ID, packet_size);

				MakeSessionDisconnectThisTime(session[index]);
			}
		}
	}
	else
	{
		/* ***************************************
		브로드 캐스트용 버퍼에서 한 번에 읽어올 수 있는 사이즈가 패킷보다 작아서
		한 번에 enqueue를 할 수 없는 상황임. 따라서 두 번에 걸쳐서 패킷을 세션의 송신버퍼에 삽입해야함.
		**************************************** */
		for (size_t index = 0; index < size; index++)
		{
			if (session[index]->_ID == except_ID)
			{
				continue;
			}

			/* ***************************************
			첫 번째 삽입.
			**************************************** */
			int enqueue_size = session[index]->_send_Q->Enqueue(g_broadcast_Q.GetReadBufferPtr(), readalbe_size_at_once);

			/* ***************************************
			세션의 센드 큐가 터진 상황. 끊어줌.
			**************************************** */
			if (enqueue_size != readalbe_size_at_once)
			{
				LOG(LOG_LEVEL_WARNING, L"Disconnect session by broadcast. socket : %I64u session_ID : %d size of packets : %d ", session[index]->_socket, session[index]->_ID, packet_size);

				DisconnectSession(session[index]);
			}

			/* ***************************************
			두 번째 삽입을 위한 여분 계산
			**************************************** */
			int left_size = packet_size - readalbe_size_at_once;

			/* ***************************************
			두 번째 삽입일 때는, 버퍼의 마지막에서 버퍼의 처음으로
			돌아온 것을 기준으로 계산된 것.
			**************************************** */
			enqueue_size = session[index]->_send_Q->Enqueue(g_broadcast_Q.GetBufferPtr(), left_size);

			/* ***************************************
			세션의 센드 큐가 터진 상황. 끊어줌.
			**************************************** */
			if (enqueue_size != left_size)
			{
				LOG(LOG_LEVEL_WARNING, L"Disconnect session by broadcast. socket : %I64u session_ID : %d size of packets : %d ", session[index]->_socket, session[index]->_ID, packet_size);

				DisconnectSession(session[index]);
			}
		}
	}

	/* ***************************************
	모든 세션에 패킷을 보내줬으므로,
	보낸만큼 땡겨줌.
	**************************************** */
	if (b_next)
	{
		g_broadcast_Q.MoveFrontAfterRead(packet_size);
		g_broadcast_except_list.pop_front();
	}
}