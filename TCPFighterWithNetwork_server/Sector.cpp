#include "stdafx.h"
#include "Protocol.h"
#include "Contents.h"
#include "SerializationBuffer.h"
#include "Session.h"
#include "Character.h"
#include "Sector.h"
#include "Log.h"
#include "NetworkProcedure.h"
#include "Profiler.h"

extern std::map <SOCKET, Session*> g_session_map;

extern std::map <int, Character*> g_character_map;

extern std::list<int> g_sector_arr[SECTOR_MAX_Y + 1][SECTOR_MAX_X + 1];

extern void(*CalculateLeavSector[9])(TargetSector*, short, short);
extern void(*CalculateEnterSector[9])(TargetSector*, short, short);
extern void TerminateServer();
extern SetOfTargetSector g_sector_info[SECTOR_MAX_Y + 1][SECTOR_MAX_X + 1];

void SetSector(Character* const character)
{
	character->_sector_x = character->_pos_x / SECTOR_SIZE;
	character->_sector_y = character->_pos_y / SECTOR_SIZE;

	g_sector_arr[character->_sector_y][character->_sector_x].push_back(character->_ID);

#ifdef _DEBUG
	wchar_t debug_string[100];
	debug_string[99] = L'\0';
	swprintf_s(debug_string, 100, L"Set Sector \n ID: %d sector_x : %d sector_y : %d \n", character->_ID, character->_sector_x, character->_sector_y);
	OutputDebugString(debug_string);
#endif // _DEBUG
}

void RemoveCharacterInSector(Character* const character)
{
	g_sector_arr[character->_sector_y][character->_sector_x].remove(character->_ID);

#ifdef _DEBUG
	wchar_t debug_string[100];
	debug_string[99] = L'\0';
	swprintf_s(debug_string, 100, L"Delete Session In Sector \n ID: %d sector_x : %d sector_y : %d \n", character->_ID, character->_sector_x, character->_sector_y);
	OutputDebugString(debug_string);
#endif // _DEBUG
}

bool IsSameSector(Character* const character)
{
	int prev_sector_y = character->_sector_y;
	int prev_sector_x = character->_sector_x;

	short current_sector_y = (character->_pos_y / SECTOR_SIZE);
	short current_sector_x = (character->_pos_x / SECTOR_SIZE);
	
	return (prev_sector_x == current_sector_x && prev_sector_y == current_sector_y);
}

bool EnterSector(Character* const character)
{
	if (!IsSameSector(character))
	{
		SetSector(character);

		return true;
	}

	return false;
}

bool LeaveSector(Character* const character)
{
	if (!IsSameSector(character))
	{
		RemoveCharacterInSector(character);

		return true;
	}

	return false;
}

void MulticastLeaveSector(int ID, short old_x, short old_y, short cur_x, short cur_y)
{
	TargetSector* target_sector;

	int dif_x = cur_x - old_x;
	int dif_y = cur_y - old_y;

	//if (abs(dif_x) > 1 || abs(dif_y) > 1)
	//{
	//	PROFILE_BEGIN(L"Not_Pre_Init_Leave");
	//	TargetSector old_sector;
	//	TargetSector current_sector;
	//	TargetSector result_sector;

	//	CalculateNeighborsOfSector(&old_sector, old_x, old_y);
	//	CalculateNeighborsOfSector(&current_sector, cur_x, cur_y);

	//	/* ***************************************
	//	old_sector에서 제외될 부분을 처리.
	//	**************************************** */
	//	for (int old_index = 0; old_index < old_sector._size; old_index++)
	//	{
	//		for (int current_index = 0; current_index < current_sector._size; current_index++)
	//		{
	//			if (current_sector._sector[current_index]._x == old_sector._sector[old_index]._x &&
	//				current_sector._sector[current_index]._y == old_sector._sector[old_index]._y)
	//			{
	//				old_sector._sector[old_index]._x = -1;
	//			}
	//		}
	//	}

	//	int result_sector_size = 0;

	//	/* ***************************************
	//	result_sector에 old_sector에서 제외되지 않는 부분을 복사.
	//	**************************************** */
	//	for (int index = 0; index < old_sector._size; index++)
	//	{
	//		if (old_sector._sector[index]._x != -1)
	//		{
	//			result_sector._sector[result_sector_size]._x = old_sector._sector[index]._x;
	//			result_sector._sector[result_sector_size]._y = old_sector._sector[index]._y;
	//			result_sector_size++;
	//		}
	//	}

	//	result_sector._size = result_sector_size;

	//	target_sector = &result_sector;

	//	PROFILE_END(L"Not_Pre_Init_Leave");
	//}
	//else
	//{
	//	PROFILE_BEGIN(L"Pre_Init_Leave");
	//	if (dif_x != 0)
	//	{
	//		int direction = 1;

	//		if (dif_x < 0)
	//		{
	//			direction = -1;
	//		}

	//		dif_x /= dif_x;
	//		dif_x *= direction;
	//	}

	//	if (dif_y != 0)
	//	{
	//		int direction = 1;

	//		if (dif_y < 0)
	//		{
	//			direction = -1;
	//		}

	//		dif_y /= dif_y;
	//		dif_y *= direction;
	//	}

	//	dif_x++;
	//	dif_y++;

	//	int index = 8 - (dif_y * 3 + dif_x);

	//	target_sector = &g_sector_info[old_y][old_x]._set_of_sectors[index];
	//	PROFILE_END(L"Pre_Init_Leave");
	//}

	PROFILE_BEGIN(L"Not_Pre_Init_Leave");
	TargetSector old_sector;
	TargetSector current_sector;
	TargetSector result_sector;

	CalculateNeighborsOfSector(&old_sector, old_x, old_y);
	CalculateNeighborsOfSector(&current_sector, cur_x, cur_y);

	/* ***************************************
	old_sector에서 제외될 부분을 처리.
	**************************************** */
	for (int old_index = 0; old_index < old_sector._size; old_index++)
	{
		for (int current_index = 0; current_index < current_sector._size; current_index++)
		{
			if (current_sector._sector[current_index]._x == old_sector._sector[old_index]._x &&
				current_sector._sector[current_index]._y == old_sector._sector[old_index]._y)
			{
				old_sector._sector[old_index]._x = -1;
			}
		}
	}

	int result_sector_size = 0;

	/* ***************************************
	result_sector에 old_sector에서 제외되지 않는 부분을 복사.
	**************************************** */
	for (int index = 0; index < old_sector._size; index++)
	{
		if (old_sector._sector[index]._x != -1)
		{
			result_sector._sector[result_sector_size]._x = old_sector._sector[index]._x;
			result_sector._sector[result_sector_size]._y = old_sector._sector[index]._y;
			result_sector_size++;
		}
	}

	result_sector._size = result_sector_size;

	target_sector = &result_sector;

	PROFILE_END(L"Not_Pre_Init_Leave");

	InformLeavingSector(ID, target_sector);
	InformSessionOfNeighborsDeleted(ID, target_sector);
}

void MulticastEnterSector(int ID, short old_x, short old_y, short cur_x, short cur_y)
{
	TargetSector* target_sector = nullptr;

	int dif_x = cur_x - old_x;
	int dif_y = cur_y - old_y;

	//if (abs(dif_x) > 1 || abs(dif_y) > 1)
	//{
	//	PROFILE_BEGIN(L"Not_Pre_Init_Enter");
	//	TargetSector old_sector;
	//	TargetSector current_sector;
	//	TargetSector result_sector;

	//	CalculateNeighborsOfSector(&old_sector, old_x, old_y);
	//	CalculateNeighborsOfSector(&current_sector, cur_x, cur_y);

	//	/* ***************************************
	//	current_sector에서 제외될 부분을 처리.
	//	**************************************** */
	//	for (int current_index = 0; current_index < current_sector._size; current_index++)
	//	{
	//		for (int old_index = 0; old_index < old_sector._size; old_index++)
	//		{
	//			if (current_sector._sector[current_index]._x == old_sector._sector[old_index]._x &&
	//				current_sector._sector[current_index]._y == old_sector._sector[old_index]._y)
	//			{
	//				current_sector._sector[current_index]._x = -1;
	//			}
	//		}
	//	}

	//	int result_sector_size = 0;

	//	/* ***************************************
	//	result_sector에 current_sector에서 제외되지 않는 부분을 복사.
	//	**************************************** */
	//	for (int index = 0; index < current_sector._size; index++)
	//	{
	//		if (current_sector._sector[index]._x != -1)
	//		{
	//			result_sector._sector[result_sector_size]._x = current_sector._sector[index]._x;
	//			result_sector._sector[result_sector_size]._y = current_sector._sector[index]._y;
	//			result_sector_size++;
	//		}
	//	}

	//	result_sector._size = result_sector_size;

	//	target_sector = &result_sector;
	//	PROFILE_END(L"Not_Pre_Init_Enter");
	//}
	//else
	//{
	//	PROFILE_BEGIN(L"Pre_Init_Enter");
	//	if (dif_x != 0)
	//	{
	//		int direction = 1;

	//		if (dif_x < 0)
	//		{
	//			direction = -1;
	//		}

	//		dif_x /= dif_x;
	//		dif_x *= direction;
	//	}

	//	if (dif_y != 0)
	//	{
	//		int direction = 1;

	//		if (dif_y < 0)
	//		{
	//			direction = -1;
	//		}

	//		dif_y /= dif_y;
	//		dif_y *= direction;
	//	}

	//	dif_x++;
	//	dif_y++;

	//	int index = dif_y * 3 + dif_x;

	//	target_sector = &g_sector_info[cur_y][cur_x]._set_of_sectors[index];
	//	PROFILE_END(L"Pre_Init_Enter");
	//}
	//


	PROFILE_BEGIN(L"Not_Pre_Init_Enter");
	TargetSector old_sector;
	TargetSector current_sector;
	TargetSector result_sector;

	CalculateNeighborsOfSector(&old_sector, old_x, old_y);
	CalculateNeighborsOfSector(&current_sector, cur_x, cur_y);

	/* ***************************************
	current_sector에서 제외될 부분을 처리.
	**************************************** */
	for (int current_index = 0; current_index < current_sector._size; current_index++)
	{
		for (int old_index = 0; old_index < old_sector._size; old_index++)
		{
			if (current_sector._sector[current_index]._x == old_sector._sector[old_index]._x &&
				current_sector._sector[current_index]._y == old_sector._sector[old_index]._y)
			{
				current_sector._sector[current_index]._x = -1;
			}
		}
	}

	int result_sector_size = 0;

	/* ***************************************
	result_sector에 current_sector에서 제외되지 않는 부분을 복사.
	**************************************** */
	for (int index = 0; index < current_sector._size; index++)
	{
		if (current_sector._sector[index]._x != -1)
		{
			result_sector._sector[result_sector_size]._x = current_sector._sector[index]._x;
			result_sector._sector[result_sector_size]._y = current_sector._sector[index]._y;
			result_sector_size++;
		}
	}

	result_sector._size = result_sector_size;

	target_sector = &result_sector;
	PROFILE_END(L"Not_Pre_Init_Enter");

	InformCharacterOfNeighbors(ID, target_sector);
	InformNeighborsOfCharacter(ID, target_sector);
}


void CalculateNeighborsOfSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	PROFILE_BEGIN(L"Pre_Init_Around");
	*target_sector = g_sector_info[cur_y][cur_x]._set_of_sectors[4];
	PROFILE_END(L"Pre_Init_Around");
}


bool CheckSectorOutOfRange(int index)
{
	return (index >= 0 && index <= SECTOR_MAX_X && index <= SECTOR_MAX_Y);
}

void InformSessionOfNeighborsDeleted(int ID, TargetSector* spread_sector)
{
	Character* character = nullptr;

	FindCharacter(&character, ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d", WFILE, __LINE__);

		TerminateServer();

		return;
	}

	int size = spread_sector->_size;

	if (size == 0)
	{
		return;
	}

	for (int index = 0; index < size; index++)
	{
		int x = spread_sector->_sector[index]._x;
		int y = spread_sector->_sector[index]._y;

		auto iter = g_sector_arr[y][x].begin();
		auto end = g_sector_arr[y][x].end();

		auto character_end = g_character_map.end();

		while (iter != end)
		{
			Character* target = nullptr;

			FindCharacter(&target, *iter);

			if (!target)
			{
				LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d", WFILE, __LINE__);

				TerminateServer();

				return;
			}

			NetDeleteCharacterInSector(character->_session, target->_ID);
			
			++iter;
		}
	}
}

void InformCharacterOfNeighbors(int ID, TargetSector* target_sector)
{
	int size = target_sector->_size;

	if (size == 0)
	{
		return;
	}

	Character* character = nullptr;

	FindCharacter(&character, ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d", WFILE, __LINE__);

		return;
	}

	for (int index = 0; index < size; index++)
	{
		int x = target_sector->_sector[index]._x;
		int y = target_sector->_sector[index]._y;

		auto iter = g_sector_arr[y][x].begin();
		auto end = g_sector_arr[y][x].end();

		auto character_end = g_character_map.end();

		while (iter != end)
		{
			auto character_iter = g_character_map.find(*iter);

			if (character_iter != character_end)
			{
				Character* around_character = character_iter->second;

				NetCreateOtherCharacterSC(character->_session, around_character->_ID, around_character->_direction, around_character->_pos_x, around_character->_pos_y, around_character->_HP);

				if (IsCharacterMove(around_character))
				{
					NetMoveStartSC(character->_session, around_character);
				}
			}

			++iter;
		}
	}
}

void InformNeighborsOfCharacter(int ID, TargetSector* spread_sector)
{
	Character* character = nullptr;

	FindCharacter(&character, ID);

	if (!character)
	{
		LOG(LOG_LEVEL_ERROR, L"Character nullptr. file : %s, line : %d", WFILE, __LINE__);
		
		TerminateServer();

		return;
	}

	int size = spread_sector->_size;

	if (size == 0)
	{
		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 10, PACKET_CREATE_OTHER_CHARACTER_SC);
	CreatePacketCreateOtherCharacterSC(&packet, ID, character->_direction, character->_pos_x, character->_pos_y, character->_HP);

	SendPacketToMultiSector(&packet, spread_sector);

	packet.Clear();
	
	if (IsCharacterMove(character))
	{
		SerializationBuffer packet;

		CreatePacketHeader(&packet, PACKET_HEADER_CODE, 9, PACKET_MOVE_START_SC);
		CreatePacketMoveStartSC(&packet, character->_ID, character->_action, character->_pos_x, character->_pos_y);

		SendPacketToMultiSector(&packet, spread_sector);
	}
}

void InformLeavingSector(int ID, TargetSector* spread_sector)
{
	int size = spread_sector->_size;

	if (size == 0)
	{
		return;
	}

	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 4, PACKET_DELETE_CHARACTER_SC);
	CreatePacketDeleteCharacterSC(&packet, ID);

	SendPacketToMultiSector(&packet, spread_sector);
}

void CalculateLeaveLeftUpSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0; 

	short target_x = old_x - 1;
	short target_y = old_y - 1;

	do
	{
		if (target_y >= 0)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = old_x - 1;
		target_y = old_y;
		
		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y++;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}
	
	} while (0);

	target_sector->_size = count;
}

void CalculateLeaveUpSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x - 1;
	short target_y = old_y - 1;

	do
	{
		if (target_y < 0)
		{
			break;
		}

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateLeaveRightUpSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x + 1;
	short target_y = old_y - 1;

	do
	{
		if (target_y >= 0)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = old_x + 1;
		target_y = old_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y++;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateLeaveLeftSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x - 1;
	short target_y = old_y - 1;

	do
	{
		if (target_x < 0)
		{
			break;
		}

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);
	
	target_sector->_size = count;
}

void CalculateLeaveCurrentSector(TargetSector* target_sector, short old_x, short old_y)
{
	short start_x = max(old_x - 1, 0);
	short start_y = max(old_y - 1, 0);
	short end_x = min(old_x + 2, SECTOR_MAX_X + 1);
	short end_y = min(old_y + 2, SECTOR_MAX_Y + 1);

	int count = 0;

	for (short y = start_y; y < end_y; y++)
	{
		for (short x = start_x; x < end_x + 2; x++)
		{
			target_sector->_sector[count++] = Sector{ y, x };
		}
	}

	target_sector->_size = count;
}

void CalculateLeaveRightSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x + 1;
	short target_y = old_y - 1;

	do
	{
		if (target_x > SECTOR_MAX_X)
		{
			break;
		}

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateLeaveLeftDownSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x - 1;
	short target_y = old_y + 1;

	do
	{
		if (target_y <= SECTOR_MAX_Y)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = old_x - 1;
		target_y = old_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y--;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateLeaveDownSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x - 1;
	short target_y = old_y + 1;

	do
	{
		if (target_y > SECTOR_MAX_Y)
		{
			break;
		}

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateLeaveRightDownSector(TargetSector* target_sector, short old_x, short old_y)
{
	int count = 0;

	short target_x = old_x + 1;
	short target_y = old_y + 1;

	do
	{
		if (target_y <= SECTOR_MAX_Y)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = old_x + 1;
		target_y = old_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y--;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterLeftUpSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x - 1;
	short target_y = cur_y - 1;

	do
	{
		if (target_y >= 0)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = cur_x - 1;
		target_y = cur_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y++;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterUpSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x - 1;
	short target_y = cur_y - 1;

	do
	{
		if (target_y < 0)
		{
			break;
		}

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterRightUpSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x + 1;
	short target_y = cur_y - 1;

	do
	{
		if (target_y >= 0)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = cur_x + 1;
		target_y = cur_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y++;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterLeftSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x - 1;
	short target_y = cur_y - 1;

	do
	{
		if (target_x < 0)
		{
			break;
		}

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterCurrentSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	short start_x = max(cur_x - 1, 0);
	short start_y = max(cur_y - 1, 0);
	short end_x = min(cur_x + 2, SECTOR_MAX_X + 1);
	short end_y = min(cur_y + 2, SECTOR_MAX_Y + 1);
	
	int count = 0;

	for (short y = start_y; y < end_y; y++)
	{
		for (short x = start_x; x < end_x + 2; x++)
		{
			target_sector->_sector[count++] = Sector{ x, y };
		}
	}

	target_sector->_size = count;
}

void CalculateEnterRightSector(TargetSector* target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x + 1;
	short target_y = cur_y - 1;

	do
	{
		if (target_x > SECTOR_MAX_X)
		{
			break;
		}

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_y++;

		if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterLeftDownSector(TargetSector * target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x - 1;
	short target_y = cur_y + 1;

	do
	{
		if (target_y <= SECTOR_MAX_Y)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x++;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = cur_x - 1;
		target_y = cur_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y--;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterDownSector(TargetSector * target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x - 1;
	short target_y = cur_y + 1;

	do
	{
		if (target_y > SECTOR_MAX_Y)
		{
			break;
		}

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

		target_x++;

		if (target_x >= 0 && target_x <= SECTOR_MAX_X)
		{
			target_sector->_sector[count++] = Sector{ target_x, target_y };
		}

	} while (0);

	target_sector->_size = count;
}

void CalculateEnterRightDownSector(TargetSector * target_sector, short cur_x, short cur_y)
{
	int count = 0;

	short target_x = cur_x + 1;
	short target_y = cur_y + 1;

	do
	{
		if (target_y <= SECTOR_MAX_Y)
		{
			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_x--;

			if (target_x >= 0 && target_x <= SECTOR_MAX_X)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

		target_x = cur_x + 1;
		target_y = cur_y;

		if (target_x >= 0)
		{
			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}

			target_y--;

			if (target_y >= 0 && target_y <= SECTOR_MAX_Y)
			{
				target_sector->_sector[count++] = Sector{ target_x, target_y };
			}
		}

	} while (0);

	target_sector->_size = count;
}
