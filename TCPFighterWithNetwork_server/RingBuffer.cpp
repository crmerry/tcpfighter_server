#include"stdafx.h"
#include "RingBuffer.h"

RingBuffer::RingBuffer(int buffer_size)
	: _size(buffer_size), _front(0), _rear(0)
{
	_buffer = new char[buffer_size];
}

RingBuffer::~RingBuffer()
{
	delete[] _buffer;
}

int RingBuffer::GetBufferSize()
{
	return _size;
}

int RingBuffer::GetUsedSize()
{
	if (_rear >= _front)
	{
		return _rear - _front;
	}
	else
	{
		return _size - _front + _rear;
	}
}

int RingBuffer::GetFreeSize()
{
	if (_rear >= _front)
	{
		return _size - _rear + _front - 1;
	}
	else
	{
		return _front - _rear - 1;
	}
}

/* ***************************************
한 번에, 버퍼에 쓸 수있는 크기
1) ㅁㅁFㅁㅁㅁRㅁㅁ,
: R 포함 3칸
2) ㅁㅁRㅁㅁㅁFㅁㅁ,
: R 포함 3칸
3) ㅁㅁFㅁㅁㅁㅁㅁR,
: R 1칸
**************************************** */
int RingBuffer::GetWritableSizeAtOnce()
{
	if (_rear >= _front)
	{
		// front가 0인 경우만 다르다.
		if (_front == 0)
		{
			return _size - _rear - 1;
		}

		return _size - _rear;
	}
	else
	{
		return _front - _rear - 1;
	}
}

/* ***************************************
한 번에 버퍼로부터 읽을 수 있는 크기
1) ㅁㅁFㅁㅁㅁRㅁㅁ
: F 포함 4칸, 다 읽으면 F,R 위치 동일
2) ㅁㅁRㅁㅁㅁFㅁㅁ
: F 포함 3칸, 다 읽으면 F는 인덱스 0으로 이동됨.
3) ㅁㅁRㅁㅁㅁㅁㅁF
: F 포함 1칸
**************************************** */
int RingBuffer::GetReadableSizeAtOnce()
{
	if (_rear >= _front)
	{
		return _rear - _front;
	}
	else
	{
		return _size - _front;
	}
}

char* RingBuffer::GetBufferPtr()
{
	return _buffer;
}

/* ***************************************
1)
ㅁㅁ F ㅁㅁ R ㅁㅁ
0 1  2 3 4  5 6 7
5번째 인덱스에 대한 포인터를 반환.
2)
ㅁㅁFㅁㅁㅁㅁR
0 1 2 3 4 5 6 7
7번째 인덱스에 대한 포인터를 반환.
**************************************** */
char* RingBuffer::GetWriteBufferPtr()
{
	return &_buffer[(_rear) % _size];
}

/* ***************************************
1)
ㅁㅁFㅁㅁRㅁㅁ
0 1 2 3 4 5 6 7
2번째 인덱스에 대한 포인터를 반환.
2)
ㅁㅁRㅁㅁㅁㅁF
0 1 2 3 4 5 6 7
7번째 인덱스에 대한 포인터를 반환.
**************************************** */
char* RingBuffer::GetReadBufferPtr()
{
	return &_buffer[(_front) % _size];
}

/* ***************************************
외부에서 GetWriteBufferPtr를 통하여 데이터에 접근할 때 사용.
Enqueue호출을 하지 않았으므로, rear가 움직이지 않기 때문.
따라서 밖에서 조정해줘야함.
**************************************** */
void RingBuffer::MoveRearAfterWrite(int size)
{
	_rear = (_rear + size) % _size;
}

/* ***************************************
외부에서 GetReadBufferPtr을 통하여 데이터에 접근할 때 사용.
Dequeue호출을 하지 않았으므로, front가 움직이지 않기 때문.
따라서 밖에서 조정해줘야함.
**************************************** */
void RingBuffer::MoveFrontAfterRead(int size)
{
	_front = (_front + size) % _size;
}

/* ***************************************
1> in_size와 GetFreeSize()를 비교하여
반영할 Enqueue사이즈 획득.
2> GetWritableSizeAtOnce()로 한 번에 Enqueue할 수 있는 양을 구하고
in_size보다 크면 바로 카피
in_size보다 작으면 분할해서 카피.
**************************************** */
int RingBuffer::Enqueue(char* data, int in_size)
{
	int usable_size = GetFreeSize();
	if (in_size > usable_size)
	{
		in_size = usable_size;
	}

	int writable_size_atonce = GetWritableSizeAtOnce();
	if (writable_size_atonce >= in_size)
	{
		memcpy_s(&_buffer[_rear], in_size, data, in_size);
		_rear = (_rear + in_size) % _size;
	}
	else
	{
		memcpy_s(&_buffer[_rear], writable_size_atonce, data, writable_size_atonce);
		int left_size = in_size - writable_size_atonce;
		memcpy_s(_buffer, left_size, data + writable_size_atonce, left_size);
		_rear = left_size;
	}

	return in_size;
}

/* ***************************************
1> out_size와 GetusedSize()를 비교하여
반영할 Dequeue사이즈 획득.
2> GetReadableSizeAtOnce()로 한 번에 Dequeue할 수 있는 양을 구하고
out_size보다 크면 바로 카피
out_size보다 작으면 분할해서 카피.
**************************************** */
int RingBuffer::Dequeue(char* data, int out_size)
{
	int used_size = GetUsedSize();

	if (out_size > used_size)
	{
		out_size = used_size;
	}

	int readable_size_atonce = GetReadableSizeAtOnce();
	if (readable_size_atonce >= out_size)
	{
		memcpy_s(data, out_size, &_buffer[_front], out_size);
		_front = (_front + out_size) % _size;
	}
	else
	{
		memcpy_s(data, readable_size_atonce, &_buffer[_front], readable_size_atonce);
		int left_size = out_size - readable_size_atonce;
		memcpy_s(data + readable_size_atonce, left_size, _buffer, left_size);
		_front = left_size;
	}

	return out_size;
}

/* ***************************************
Dequeue와 같지만, front 이동 없음.
**************************************** */
int RingBuffer::Peek(char * data, int out_size)
{
	int used_size = GetUsedSize();

	if (out_size > used_size)
	{
		out_size = used_size;
	}

	int once_write_size = GetReadableSizeAtOnce();
	if (once_write_size >= out_size)
	{
		memcpy_s(data, out_size, &_buffer[_front], out_size);
	}
	else
	{
		memcpy_s(data, once_write_size, &_buffer[_front], once_write_size);
		int left_size = out_size - once_write_size;
		memcpy_s(data + once_write_size, left_size, _buffer, left_size);
	}

	return out_size;
}

void RingBuffer::ClearBuffer()
{
	_front = 0;
	_rear = 0;
}