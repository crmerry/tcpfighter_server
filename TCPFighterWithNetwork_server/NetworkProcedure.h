#pragma once

struct Session;
struct TargetSector;
class SerializationBuffer;

/* ***************************************
구조체
**************************************** */
struct BroadCastUnit
{
	int	_except_ID;
	int	_packet_size;
};

/* ***************************************
패킷 생성
**************************************** */
void CreatePacketHeader(SerializationBuffer* const packet, unsigned char code, unsigned char size, unsigned char type);
void CreatePacketCreateMyCharacterSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y, unsigned char HP);
void CreatePacketCreateOtherCharacterSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y, unsigned char HP);
void CreatePacketDeleteCharacterSC(SerializationBuffer* const packet, int ID);
void CreatePacketMoveStartSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketMoveStopSC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack1SC(SerializationBuffer* const packet, int ID, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketDamageSC(SerializationBuffer* const packet, int attack_ID, int damage_ID, unsigned char damaga_HP);
void CreatePacketSyncSC(SerializationBuffer* const packet, int ID, unsigned short x, unsigned short y);
void CreatePacketEchoSC(SerializationBuffer* const packet, unsigned int time);

/* ***************************************
네트워크 프로시져
**************************************** */
void NetCreateMyCharacterSC(Session* const session);
void NetCreateOtherCharacterSC(Session* const session, int ID, unsigned char direction, unsigned short x, unsigned short y, unsigned char HP);
void NetInformSessionOfNeighbors(Session* const session);
void NetInformNeighborsOfSession(Session* const session);
void NetInformNeighborsOfSessionAboutThisPacket(Session* const session, SerializationBuffer* const packet, bool except_my_character = true);
void NetDeleteCharacterInSector(Session* const session, int ID);
void NetMoveStartCS(SerializationBuffer* const packet, Session* const session);
void NetMoveStartSC(Session* const session);
void NetMoveStartSC(Session* const session, Character* const character);
void NetMoveStopCS(SerializationBuffer* const packet, Session* const session);
void NetMoveStopSC(Session* const session);
void NetAttack1CS(SerializationBuffer* const packet, Session* const session);
void NetAttack1SC(Session* const session);
void NetAttack2CS(SerializationBuffer* const packet, Session* const session);
void NetAttack2SC(Session* const session);
void NetAttack3CS(SerializationBuffer* const packet, Session* const session);
void NetAttack3SC(Session* const session);
void NetDamageSC(Session* const attack_session, Session* const damage_session);
void NetSyncSC(Session* const session, int ID, unsigned short x, unsigned short y, bool except_my_session);
void NetEchoCS(SerializationBuffer* const packet, Session* const session);
void NetSyncPosBetweenClientAndServer(Session* const session, Character* const character, unsigned short client_x, unsigned short client_y, unsigned char packet_type);

/* ***************************************
네트워크 수신부
**************************************** */
void RecvProc(Session* const session);
bool RecvPacket(Session* const session);
bool DecodePacket(unsigned short* const type, SerializationBuffer* const packet, Session* const session);
void ProcPacket(unsigned short type, SerializationBuffer* const packet, Session* const session);

/* ***************************************
네트워크 송신부
**************************************** */
bool SendProc(Session* const session);
void SendPacketByUnicast(SerializationBuffer* const packet, Session* const session);
void SendPacketByBroadcast(SerializationBuffer* const packet, int except_ID);
void SendPacketToOneSector(SerializationBuffer* const packet, short sector_x, short sector_y, Session* const except_session = nullptr);
void SendPacketToMultiSector(SerializationBuffer* const packet, TargetSector* const target_sector, Session* const except_session = nullptr);
void SendProcBroadcastPacketByDistribute(Session** session, size_t size, int packet_size, bool b_next);